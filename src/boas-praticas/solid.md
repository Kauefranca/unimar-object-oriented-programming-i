# SOLID

> O princípio SOLID é um conjunto de cinco princípios de design de software que foram introduzidos por Robert C. Martin para orientar o desenvolvimento de código limpo, flexível e de fácil manutenção. Cada letra do acrônimo SOLID representa um princípio específico, e juntos eles formam uma base sólida para criar sistemas robustos e extensíveis.
>
> Aqui está uma introdução geral sobre cada princípio SOLID:
>
> S - Princípio da Responsabilidade Única (Single Responsibility Principle):
> O SRP enfatiza que uma classe deve ter apenas uma única responsabilidade e motivo para mudar. Isso significa que cada classe deve se concentrar em fazer uma coisa e fazer bem, tornando o código mais coeso, mais fácil de entender e manter.
>
> O - Princípio do Aberto/Fechado (Open/Closed Principle):
> O OCP destaca que as entidades de software (classes, módulos, etc.) devem estar abertas para extensão, mas fechadas para modificação. Isso significa que devemos projetar nossos sistemas de forma que seja possível adicionar novos comportamentos sem alterar o código existente.
>
> L - Princípio da Substituição de Liskov (Liskov Substitution Principle):
> O LSP enfatiza que os objetos de uma classe derivada devem ser substituíveis por objetos de sua classe base sem quebrar o programa. Isso garante que a herança seja usada de forma consistente e que as classes derivadas não quebrem as expectativas das classes base.
>
> I - Princípio da Segregação de Interface (Interface Segregation Principle):
> O ISP preconiza que muitas interfaces específicas são melhores do que uma única interface geral. Isso significa que uma classe não deve ser forçada a depender de interfaces que não utiliza. Isso promove a coesão e evita acoplamentos desnecessários.
>
> D - Princípio da Inversão de Dependência (Dependency Inversion Principle):
> O DIP estabelece que as classes de alto nível não devem depender de classes de baixo nível, mas ambas devem depender de abstrações. Isso incentiva a criação de um código mais flexível e reutilizável, onde as classes de alto nível não estão vinculadas a implementações específicas de classes de baixo nível.
>
> Ao aplicar os princípios SOLID em nossos projetos de software, buscamos criar sistemas mais flexíveis, fáceis de manter, extensíveis e de alta qualidade. Cada princípio SOLID destaca uma abordagem específica para o desenvolvimento de software orientado a objetos, encorajando boas práticas de design e evitando armadilhas comuns que podem levar a código complexo e frágil.
> [^chat-gpt]

---

[^chat-gpt]: <https://chat.openai.com/>
