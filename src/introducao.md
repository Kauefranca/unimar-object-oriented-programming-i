# Introdução

--- Explique para uma criança o que é programação orientada a objetos

> Claro! Vou explicar o que é programação orientada a objetos (POO) de uma forma simples.
>
> Imagine que você tem um conjunto de peças de Lego. Cada peça tem uma forma específica e pode ter diferentes cores. Você pode usar essas peças para construir diferentes objetos, como casas, carros ou robôs.
>
> Na programação orientada a objetos, é como se estivéssemos construindo objetos usando peças de Lego. Cada objeto é formado por duas coisas: propriedades e ações.
>
> As propriedades são características que o objeto possui. Por exemplo, um carro pode ter propriedades como cor, modelo e velocidade. Já as ações são coisas que o objeto pode fazer. Por exemplo, um carro pode acelerar, frear e virar.
>
> Na programação orientada a objetos, criamos estruturas chamadas "classes" que definem como os objetos serão construídos. Uma classe é como uma espécie de molde que descreve as propriedades e ações que os objetos daquela classe terão.
>
> Por exemplo, podemos ter uma classe chamada "Carro" que define como um carro será construído. Essa classe pode ter propriedades como cor e modelo, e ações como acelerar e frear. Quando queremos criar um objeto específico, chamamos isso de "instanciar" a classe. É como pegar o molde da classe e criar um objeto real com base nele.
>
> Assim, a programação orientada a objetos nos permite organizar nosso código de maneira mais fácil e lógica, pensando em termos de objetos com propriedades e ações. Podemos reutilizar as classes para criar vários objetos semelhantes e modificar apenas as propriedades e ações necessárias.
>
> Então, resumindo, a programação orientada a objetos é como construir objetos usando peças de Lego. Cada objeto tem suas características (propriedades) e coisas que pode fazer (ações). Criamos classes como moldes para definir como os objetos serão construídos, e isso nos ajuda a organizar e reutilizar nosso código de forma eficiente.
> [^chat-gpt]

---

<!-- @TODO Simula 67 -->
<!-- @TODO C++ -->

Muitos dizem que usamos programação orientada a objetos porque estamos habituados com a ideia de objetos na vida real,
então seria mais simples pensar em programação como os objetos do mundo real.
O que não deixa de ser uma verdade, porém, muitas vezes precisamos pensar em objetos que não existem no mundo real, apenas no mundo virtual, apenas na abstração que elaboramos e programamos.

[Alan Kay](https://pt.wikipedia.org/wiki/Alan_Kay) criador da linguagem smalltalk, uma das primeiras linguagem orientada a objetos, disse:

>... o computador ideal deveria funcionar como um organismo vivo,
> isto é, cada "célula" comportar-se-ia relacionando-se com outras a fim de alcançar um objetivo,
> contudo, funcionando de forma autônoma.
> As células poderiam também reagrupar-se para resolver um outro problema ou desempenhar outras funções.
> [^alan-kay]

Para mim essa é a chave para a POO, devemos desmembrar nossos problemas em problemas menores,
criar pequenos "organismos vivos" para resolver cada uns desses pequenos problemas
e finalmente agrupar esses organismos para resolver problemas maiores.

Agrupar esses organismos nem sempre é simples,
eles precisam se encaixar,
assim como peças de um quebra cabeças ou blocos de lego.
Ter um bom design orientado a objetos significa que temos bons encaixes,
as peças conseguem se conectar de maneiras diferentes para resolver problemas diferentes.
Bons encaixes permitem que reusemos nossas peças de diversas formas.

A POO não exclui os outros paradigmas de programação,
normalmente combinamos o que tem de melhor em cada paradigma.
Tudo que já sabemos sobre programação estruturada pode ser usado dentro da POO,
também podemos usar vários elementos de programação funcional.

[^chat-gpt]: <https://chat.openai.com/>

[^alan-kay]: <https://pt.wikipedia.org/wiki/Alan_Kay>
