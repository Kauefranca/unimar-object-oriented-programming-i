# Atributos

Classes são como metadados e os objetos os dados, as classes vão definir o formato e comportamento de todos os objetos daquele tipo.
Podemos imaginar que uma classe é como uma planilha e suas colunas, e que os objetos são as linhas preenchendo essas colunas.
A classe definiu quais colunas existem e o tipo de informação que queremos em cada uma delas e cada objeto preenche as informações nas colunas.
Em POO chamamos as colunas de **atributos**.

Vamos exemplificar criando uma classe `Retangulo`, com os atributos, ou colunas, largura e comprimento.

```python
from dataclasses import dataclass

@dataclass
class Retangulo:
    largura: float
    comprimento: float
```

Então a classe `Retangulo` define que todo objeto do tipo `Retangulo` tem largura e comprimento do tipo `float`.
Podemos imaginar uma tabala com essas colunas e para fins didáticos vamos adicionar uma coluna com o nome da variável.

| Variável | Largura | Comprimento |
|----------| --------| ----------- |

Com o tipo `Retangulo` definido podemos criar ou instanciar nossos objetos.

```python
a = Retangulo(5, 25)
```

Neste momento estamos utilizando a variável `a` para armazenar nosso novo objeto do tipo `Retangulo`, com comprimento `5` e largura `25`.

| Variável | Largura | Comprimento |
|----------| --------| ----------- |
|    a     |    5    |      25     |

Podemos criar mais objetos e cada um tem de forma independente seus atributos largura e comprimento.

```python
a = Retangulo(5, 25)
b = Retangulo(10, 15)
c = Retangulo(8, 18)
```

| Variável | Largura | Comprimento |
|----------| --------| ----------- |
|    a     |    5    |      25     |
|    b     |    10   |      15     |
|    c     |    8    |      18     |

Para acessar os atributos de cada objeto basta adicionarmos o simbolo `.` e o nome do atributo logo após o nome da variável.
O simbolo pode variar dependendo da linguagem de programação, os mais comuns são o `.` e o `->`.

```python
a.largura 
b.comprimento
```

O mesmo exemplo em Java:

```java
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Retangulo {
    public float largura;
    public float comprimento;
}
```

Em java usamos `new` para criar novos objetos.

```java
var a = new Retangulo(5, 25);
var b = new Retangulo(10, 15);
var c = new Retangulo(8, 18);
```

Com UML podemos representar de forma gráfica assim:

```plantuml
@startuml

class Retangulo {
    + largura: float
    + comprimento: float
}


object a {
    largura = 5
    comprimento =  25
}

object b {
    largura = 10
    comprimento =  15
}

object c {
    largura = 8
    comprimento =  18
}

a ..|> Retangulo: Instância de
b ..|> Retangulo: Instância de
c ..|> Retangulo: Instância de

@enduml
```

As variáveis `a`, `b` e `c` são objetos do tipo `Retangulo`, cada um deles com seus próprios atributos.

## Atributos Estáticos

Atributos estáticos são independente das instâncias,
na verdade são atributos da própria classe.
Em python, por exemplo, as classes também são objetos,
então encarar como um atributo da classe faz muito sentido.
Também pode-se dizer que é um atributo compartilhado entre todos os objetos daquele tipo, tendo assim apenas um valor para todos.

## Java

```java

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Retangulo {

    public static final int LADOS = 4;
    public float largura;
    public float comprimento;

    public static void main(String... args) {
        System.out.println("Um Retangulo tem " + Retangulo.LADOS + " lados");
    }

}
```

Nesse exemplo o atributo `LADOS` é estático (`static`) e `final`,
`final` significa que não vamos alterar seu valor,
pois um retângulo sempre tem 4 lados.
Mas também podemos fazer atributos estáticos não finais.

Para acessar o atributo `LADOS` usamos o nome da classe seguido do nome do atributo,
assim como acessamos atributos em objetos,
mas agora é um atributo da classe.

## C++

Em C++ também criamos atributos estáticos quase que da mesma maneira,
mas para acessar usamos `::` invés de `.`.

```cpp
#include <iostream>

class Retangulo {
public:
    static const int LADOS = 4;
    float largura;
    float comprimento;
};

int main() {
  std::cout << Retangulo::LADOS << std::endl;
}
```

Neste exemplo usamos a palavra reservada `const`,
isso significa que é um atributo ou variável constante ou imutável,
bem semelhante a palavra reservada `final` no java.

<https://en.cppreference.com/w/cpp/language/static>

## Python

Em python não temos a palavra reservada `static`,
mas podemos declarar atributos estáticos quase que da mesma forma que declaramos atributos comuns.
As grandes diferenças estão no momento de inicialização e de acesso á esses atributos.
Para realmente entender como fazer isso em python vamos precisar ver métodos construtores, então iremos retomar esse assunto mais a frente.

<https://docs.python.org/3/tutorial/classes.html#class-and-instance-variables>

Em todos os casos, atributos estáticos existem independentemente das instâncias,
ele é criado juntamente com a classe.
Em nenhum dos exemplos anteriores precisamos criar um objeto para acessar os atributos estáticos,
o acessamos usando a classe.

## Composição e Agregação

Os tipos dos atributos também podem ser outras classes,
não somente tipos primitivos.
Considere o código em python a seguir.

```python
from dataclasses import dataclass

@dataclass
class Pessoa:
    nome: str

@dataclass
class Motor:
    tipo: str

@dataclass
class Carro:
    motor: Motor
    dono: Pessoa
```

Nesse exemplo, a classe carro tem os atributos `motor` e `dono`,
sendo `motor` do tipo `Motor`, e `dono` do tipo `Pessoa`.
Por esse motivo a classe `Carro` depende das classes `Motor` e `Pessoa`.
Se apagarmos qualquer uma dessas classes vamos ter um erro.

```plantuml
@startuml

class Pessoa {
    + nome: str
}

class Motor {
    + tipo: str
}

class Carro {
    + motor: Motor
    + dono: Pessoa
}


Carro --> Motor: "Depende de"
Carro --> Pessoa: "Depende de"

@enduml
```

Podemos também pensar que um carro sem um motor ou sem um dono não tem relevância nenhuma, algo impossível de existir no nosso sistema.
Nesse caso, podemos dizer que as classes `Motor` e `Pessoa` compõem nossa classe `Carro`.

```plantuml
@startuml

class Pessoa {
    + nome: str
}

class Motor {
    + tipo: str
}

class Carro {
    + motor: Motor
    + dono: Pessoa
}


Motor --* Carro: "Compõem"
Pessoa --* Carro: "Compõem"

@enduml
```

Caso o sistema considere carros sem motor e sem donos como casos válidos, dizemos que `Pessoa` e `Motor` agregam em nossa classe `Carro`.

```plantuml
@startuml

class Pessoa {
    + nome: str
}

class Motor {
    + tipo: str
}

class Carro {
    + motor: Motor
    + dono: Pessoa
}


Carro --o Motor: "Agrega"
Carro --o Pessoa: "Agrega"

@enduml
```

Responder se uma classe deve agregar ou compor a outra depende muito do contexto do seu problema e de como estamos abstraindo ele.
Provavelmente vamos revisitar esses conceitos diversas vezes ainda.

Imagine a situação que temos uma classe `Pessoa`,
podemos pensar que todas as pessoas precisam ter um nome, CPF e RG.
Para simplificar vamos falar apenas do nome, toda pessoa tem um nome,
logo nome compõem pessoa.
Em algum momento um usuário no nosso sistema começa a preencher um formulário com os dados de uma pessoa, mas infelizmente ele deixou o nome por último.
Se estivermos usando a classe `Pessoa` para armazenar os dados do formulário, simplesmente nós não poderíamos armazenar todos esses dados enquanto ele não fornecesse um nome.
Nós não poderíamos criar um objeto do tipo `Pessoa` sem um nome.
Dependendo da situação poderíamos resolver esse empasse criando uma classe `FormularioPessoa`, ou apenas transformar o nome em uma agregação.

Isso é um dos motivos para eu dizer para tomar cuidado ao abstrair objetos pensando no mundo real.
Várias coisas no mundo real tem cara de composição.
Não me serve de nada um carro sem rodas,
mas em um sistema de oficina ou de desmanche é muito válido.

---

- [Atributos em PHP](https://www.php.net/manual/en/language.oop5.properties.php)  
- [Atributos e Métodos Estáticos em PHP](https://www.php.net/manual/en/language.oop5.static.php)  
- [Relações com UML](https://www.guru99.com/uml-relationships-with-example.html)

---

## Exercícios de fixação

{{#quiz quiz-atributos.toml}}

## Exercícios Práticos

Recomendo que os exercícios práticos sejam feitos em python utilizando `@dataclasss`.

1. Dados de um circulo.

    Defina uma classe para o circulo com o atributo raio.

1. Dados de um quadrado.

    Defina uma classe para o quadrado com o atributo lado.

## Projeto Integrador

Recomendo a utilização de [plantuml](https://plantuml.com/) para os digramas.

1. Esboce digramas [UML das classes](https://plantuml.com/class-diagram) com seus atributos

1. Esboce diagrams [UML dos objetos](https://plantuml.com/object-diagram) com  possíveis dados como exemplo

1. Debata com os demais integrantes dos grupos, se necessário atualize seus diagramas

---

## Dúvidas frequentes

- Por quê `@dataclass` e `@AllArgsConstructor`?

    O [dataclass](https://docs.python.org/3/library/dataclasses.html) é uma [função decoradora](https://docs.python.org/3/glossary.html#term-decorator) e o
    [`@AllArgsConstructor`](https://projectlombok.org/) uma [anotação](https://docs.oracle.com/javase/tutorial/java/annotations/index.html) do [`lombok`](https://projectlombok.org/api/lombok/AllArgsConstructor).
    Ambos estão sendo usados para gerar o método construtor, que será visto a seguir, de forma automática, assim podemos focar somente nos atributos.

- Por quê `public`?

    O `public` é um **modificador de acesso**, significa que qualquer outro objeto tem acesso a esse atributo.
    Modificadores de acesso serão abordados mais adiante.

- Como decidir os atributos?

    Precisamos abstrair o problema e localizar dados que devem ser agrupados.
    Minha dica é, dados que trabalham juntos devem permanecer juntos.
    Se estou usando uma data, provavelmente preciso de dia, mês e ano.
    Se eu modificar o dia, talvez isso afete o mês, ou até mesmo o ano.
    Se estou usando um horário, provavelmente preciso de horas, minutos e segundos.
    Se eu incrementar os segundos, talvez isso afete os minutos, ou até mesmo as horas.
