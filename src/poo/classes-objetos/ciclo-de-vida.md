# Ciclo de Vida

Para tentar deixar mais claro quando o método construtor e destrutor são utilizados foram preparados alguns exemplos.
Nesses exemplos podemos dar nomes aos objetos e esses nomes são impressos durante a construção e durante a destruição.

## Java

Neste exemplo adicionamos o modificador `static` para o atributo `INSTANCE`, isso significa que esse atributo pertence a classe e não a um objeto,
também podemos encarar como se esse atributo fosse compartilhados entre todos os objetos dessa classe.
O mesmo modificador foi utilizado no método `main`, que é um padrão no java para ser o ponto de entrada do programa.

```java
<!-- cmdrun cat examples/ciclo-de-vida/Main.java -->
```

```console
<!-- ocirun openjdk:8 cd examples/ciclo-de-vida/ && javac Main.java && java Main -->
```

A primeira construção foi do objeto estático `INSTANCE`, ele foi construído assim que a classe foi definida e em nenhum momento ele foi destruído.
Ele foi removido da memoria junto com o programa, assim o coletor de lixo não teve oportunidade de executar o método `finalize`.

A segunda construção foi do objeto `a`, primeira chamada ao `new Main` no método `main`.
A atribuição de `null` para a variável `a` fez com que não tivéssemos mas nenhuma referência ao objeto `a` em nosso programa, dessa forma ao chamarmos o coletor de lixo com `System.gc()` o objeto `a` foi destruído.

A terceira construção foi do objeto `b`, segunda chamado ao `new Main` no método `main`.
Quando chamamos o coletor de lixo ainda tínhamos uma referência ao objeto e por isso ele não foi destruído.
Fizemos uma atribuição de `null` para a variável `b` logo após a coleta de lixo, assim ficamos sem referencias para esse objeto o tornando apto para coleta na proxima execução.
Como o programa finalizou logo sem seguida a JVM não teve tempo de executar nenhuma coleta de lixo antes de ser removida da memoria.

Podemos ver assim que a utilização do método destrutor em java não é muito confiável,
uma alternativa melhor para liberar recursos após o uso do objeto é com o a interface [`AutoCloseable`](https://docs.oracle.com/javase/8/docs/api/java/lang/AutoCloseable.html).

## C++

Em c++ não temos um coletor de lixo, isso significa que temos que gerenciar manualmente a destruição, ou que assim que a variável sair de escopo, ela será destruída.

```c++
<!-- cmdrun cat examples/ciclo-de-vida/test.cpp -->
```

```console
<!-- ocirun gcc cd examples/ciclo-de-vida/ && gcc -x c++ test.cpp -lstdc++ -o test.run && ./test.run -->
```

A primeira construção e ultima destruição é da variável de escopo global, ela inicia e finaliza junto com o programa.

A segunda construção é do objeto `c` que foi iniciado com `new`, que em c++ significa q precisamos usar o `delete` para remove-lo da memoria.

As demais construções foram dos objetos `a` e `b`, que foram destruídos em ordem inversa, um comportamento de pilha.
Estes foram destruídos no fim da execução da função `main`, dona do escopo o qual essas variáveis pertencem.

## Python

Em python também temos um coletor de lixo, mas sua estrategia é um pouco diferente do java.

```python
<!-- cmdrun cat examples/ciclo-de-vida/test.py -->
```

```console
<!-- ocirun python python examples/ciclo-de-vida/test.py -->
```

Podemos observar que mesmo sem chamar o coletor de lixo o objeto foi destruído assim que perdemos sua referência com `a = None`.

## PHP

Em PHP também temos coletor de lixo e sua estrategia é bem semelhante ao do python.

```php
<!-- cmdrun cat examples/ciclo-de-vida/test.php -->
```

```console
<!-- ocirun php php examples/ciclo-de-vida/test.php -->
```
