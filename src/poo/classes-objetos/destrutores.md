
# Método Destrutor

O método destrutor é utilizado quando a memória vai ser liberada, quando deixamos de usar a variável.
Em linguagens com [coletor de lixo](https://pt.wikipedia.org/wiki/Coletor_de_lixo_(inform%C3%A1tica)) geralmente temos um contador de referências implícito
e quando não temos mais nenhuma referência para o objeto o mesmo é liberado da memória.
Esse é o caso do Python, Java e PHP, já com C++ o gerenciamento de memória é parcialmente responsabilidade do programador, então muitas vezes temos que liberar a memória manualmente.
Nos dois casos os métodos destrutores são chamados, mas em linguagens com coletor de lixo é mais complicado saber quando exatamente isso acontecerá.

O nome do método destrutor pode variar de linguagem para linguagem,
em Python temos o [`__del__`](https://docs.python.org/3/reference/datamodel.html#object.__del__),
em PHP o [`__destruct`](https://www.php.net/manual/en/language.oop5.decon.php),
em Java o [`finalize`](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#finalize--)
e em C++ o [`~class-name`](https://en.cppreference.com/w/cpp/language/destructor).

## Python

```python
class Retangulo:

    largura: float
    comprimento: float

    def __del__(self):
        pass
```

## Java

```java
class Retangulo {

    public float largura;
    public float comprimento;

    protected void finalize() throws Throwable {

    }
}
```

O método `finalize` foi descontinuado a partir do java 9,
a chamada desse método nunca foi muito estável,
já que em java não podemos prever quando o coletor de lixo será executado.

## PHP

```php
class Retangulo {

    public float $largura;
    public float $comprimento;

    function __destruct() {

    }
}
```

## C++

```cpp
class Retangulo {
public:
    float largura;
    float comprimento;

    virtual ~Retangulo() {

    }
}
```
