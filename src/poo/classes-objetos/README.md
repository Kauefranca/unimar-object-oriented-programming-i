# Classes e Objetos

## Classes

— Explique para uma criança o que são classes em programação orientada a objetos

> Claro! Vou explicar o que são classes em programação orientada a objetos de uma forma simples.
>
> Imagine que você quer construir uma casa de brinquedo usando peças de Lego. Você pode criar um conjunto de instruções que define como a casa deve ser montada. Essas instruções dizem onde colocar cada peça e como elas se encaixam para formar a casa.
>
> Em programação orientada a objetos, uma classe é como esse conjunto de instruções para criar objetos. É como um modelo ou um plano que define como um objeto deve ser criado e como ele deve se comportar.
>
> Uma classe é composta por características (também chamadas de atributos) e ações (também chamadas de métodos).
>
> Vamos imaginar que estamos criando uma classe chamada "Carro". Essa classe pode ter características como cor, marca e modelo. Além disso, ela pode ter ações como acelerar, frear e virar.
>
> Quando criamos um objeto a partir de uma classe, chamamos esse objeto de uma instância da classe. Por exemplo, podemos criar um objeto chamado "meuCarro" que é uma instância da classe "Carro". Esse objeto terá suas próprias características (como cor, marca e modelo) e poderá executar as ações definidas na classe (como acelerar, frear e virar).
>
> As classes nos permitem organizar e estruturar nossos programas de uma forma mais fácil de entender. Elas nos ajudam a reutilizar código e a criar objetos com características e comportamentos específicos.
>
> Imagine que você pode criar diferentes casas de brinquedo seguindo as mesmas instruções básicas, mas alterando as cores, tamanhos e estilos das peças. Da mesma forma, as classes nos permitem criar objetos semelhantes com características únicas.
>
> Em resumo, uma classe em programação orientada a objetos é como um conjunto de instruções ou um plano que define como um objeto deve ser criado e como ele deve se comportar. É como um modelo que nos ajuda a criar objetos com características específicas e ações definidas. É uma forma de organizar e estruturar nossos programas para que possamos reutilizar código e criar objetos com comportamentos únicos.
> [^chat-gpt]

---

## Objetos

— Explique para uma criança o que são objetos em programação orientada a objetos

> Claro! Vou explicar o que são objetos em programação orientada a objetos de uma forma simples.
>
> Imagine que você tem um bichinho de pelúcia favorito, como um urso de pelúcia. Esse urso tem uma cor, um tamanho, um nome e pode fazer coisas, como pular e dançar.
>
> Em programação orientada a objetos, um objeto é como esse bichinho de pelúcia. É uma representação de algo no mundo real que possui características e pode realizar ações específicas.
>
> Os objetos são criados a partir de uma classe, que é como um modelo ou uma descrição de como o objeto deve ser. A classe define quais características o objeto terá e quais ações ele pode executar.
>
> Por exemplo, podemos ter uma classe chamada "UrsoDePelúcia" que define como um urso de pelúcia deve ser criado. Essa classe pode especificar as características do urso, como cor, tamanho e nome, e as ações que ele pode fazer, como pular e dançar.
>
> Quando criamos um objeto a partir dessa classe, estamos fazendo uma instância da classe. É como se pegássemos o modelo da classe e criássemos um urso de pelúcia real com base nele. Esse objeto tem suas próprias características, como uma cor específica, um tamanho determinado e um nome escolhido.
>
> Assim como o seu bichinho de pelúcia favorito, os objetos em programação podem ter características únicas e executar ações específicas. Podemos interagir com esses objetos, mudar suas características e fazer com que eles realizem suas ações definidas.
>
> Os objetos são muito úteis na programação porque nos permitem organizar informações de forma mais natural e representar objetos do mundo real em nosso código.
>
> Em resumo, um objeto em programação orientada a objetos é como um bichinho de pelúcia ou qualquer objeto no mundo real. Ele possui características específicas e pode realizar ações. Os objetos são criados a partir de uma classe, que é como um modelo que define como o objeto deve ser. Eles nos permitem organizar informações e representar objetos do mundo real em nosso código de maneira fácil e flexível.
> [^chat-gpt]

---

De forma sucinta, utilizamos classes para criar nossos próprios tipos dentro da linguagem de programação e os objetos são os valores que atribuímos para nossas variáveis.

Por exemplo, se consideramos todos os tipos que já conhecemos como classes, quando escrevemos `x : int = 1`, `x` é um objeto do tipo `int` e `int` é uma classe.
O objeto `x` contém o valor `1`, como ele é do tipo `int` podemos usar todas as operações que o tipo `int` provê, como somar, multiplicar e etc.
Também podemos dizer que `x` ou `1` são instâncias da classe `int`.
Quando criamos a variável `x` estamos instanciado um novo objeto.

Nem todas as linguagens tratam todos os tipos como classes.
A separação mais comum são para o que chamamos de tipos primitivos, estes são basicamente os tipos que aprendemos logo no começo, como o `char`, `int`, `float` e etc.
Textos/string são um caso a parte, algumas linguagens tem tipos primitivos para string mas também tem classes.
Em java, por exemplo, temos a classe [String](https://docs.oracle.com/javase/8/docs/api/java/lang/String.html), mas também podemos usar um vetor de caracteres ( `char[]` ), porém, a classe String fornece bem mais utilidades do que o tipo `char[]`. Já em python, todos os tipos são classes e tudo é um objeto.
Isso poderia ser uma introdução a ideia de linguagens "puramente orientada a objetos", mas não acredito que esse tipo de discussão vá melhorar as capacidades de programação de alguém no momento.

## Nome das Classes

Assim como temos regras e convenções para nomes de variáveis, também temos regras e convenções para nome de classes.
As regras normalmente são as mesmas que temos para variáveis,

Em Java, C++ e Python temos a mesma convenção, `UpperCamelCase`,
começamos  e separamos as palavras com letra maiúscula.
As classes nativas, as que já existem na linguagem, em C++ e Python as vezes não seguem essa convenção.
Em C++, por exemplo, temos a classe [`std::string`](https://cplusplus.com/reference/string/string/), e em Python, por exemplo, temos a classe [`str`](https://docs.python.org/3/library/stdtypes.html#textseq).
Mas aconselho sempre seguir a convenção quando estamos definindo nossas próprias classes.
Já em Java todas as classes nativas seguem a convenção,
por exemplo, a classe [`String`](https://docs.oracle.com/javase/8/docs/api/java/lang/String.html).
Mas os [tipos primitivos](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html) ainda usam letras minusculas.

Geralmente o nome das classes são substantivos, pois representam o tipo de um objeto.
A classe `Carro`, por exemplo, define os dados e como os objetos do tipo `Carro` vão se comportar.
Muitas vezes precisamos usar substantivos derivados das ações que queremos executar, como trabalhador, entregador e etc.
Frequentemente também deixamos esses substantivos mais específicos, adicionando alguns adjetivos, como trabalhador remoto ou entregador de pizzas.

No dia a dia, diria que nome de classes comuns seriam,
`Validator` ( Validador ), `Factory` ( Fabrica ), `Strategy` ( Estratégia), ou até mesmo `Car` ( Carro ), se estivermos desenvolvendo algum sistema que envolva carros.
Também temos derivados, adicionando alguns adjetivos, `StringValidator` ( Validador de String ), `ButtonFactory` ( Fabrica de botões), `DeliveryStrategy` ( Estratégia de Entrega ), `FakeCar` ( Carro de mentira) e etc.

Tenho preferencia por nomes de classes em inglês,
evita muito o uso da palavra "de".
Imagine a tradução de `FakeProductDeliveryStrategy`, estratégia **de** entrega **de** produto **de** mentira, são muitos "de"s.
Código em inglês também pode ser lido por mais pessoas, já que é a lingua mais falada no mundo.

## Nome dos Objetos

Basicamente seguimos as mesmas regras que seguimos com variáveis, a final, os objetos vão ficar armazenados em variáveis.
Temos algumas convenções dependendo do escopo desses objetos, mas veremos isso mais adiante.

Para reforçar, variáveis iniciam com letras minusculas, isso ajuda a não confundir uma classe com um objeto.

É muito comum usarmos o nome da classe no objeto, isso quando temos apenas um objeto daquele tipo no escopo.

```cpp
Servo servo;
```

[^servo-ino]

Nesse código em C++, temos o tipo `Servo` com **S** maiúsculo, e uma variável `servo` com **s** minusculo.
Ou podemos dizer que temos a classe `Servo` e o objeto `servo`.

O equivalente em python seria:

```python
servo = Servo();
```

E em java:

```java
Servo servo = new Servo();
```

---

[^chat-gpt]: <https://chat.openai.com/>

[^servo-ino]: [Exemplo com Arduino e Servo Motor](https://github.com/arduino-libraries/Servo/blob/master/examples/Knob/Knob.ino)

---

## Dúvidas frequentes
