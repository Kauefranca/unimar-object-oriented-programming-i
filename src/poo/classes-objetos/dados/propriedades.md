# Propriedades

Quando falamos de propriedades estamos falando de "atributos", geralmente públicos, dos nossos objetos.
Essas propriedades além de se comportar como atributos, algumas vezes a leitura ou escrita podem disparar ações.

```python
class Pessoa:
    def __init__(self, nome, idade):
        self._nome = nome
        self._idade = idade

    @property
    def nome(self):
        print("Acessando o nome...")
        return self._nome

    @nome.setter
    def nome(self, novo_nome):
        print("Alterando o nome...")
        self._nome = novo_nome

    @property
    def idade(self):
        print("Acessando a idade...")
        return self._idade

    @idade.setter
    def idade(self, nova_idade):
        print("Alterando a idade...")
        self._idade = nova_idade

pessoa1 = Pessoa("Alice", 30)

# Acessando o atributo nome (chama o método nome())
print(pessoa1.nome) # Saída: Acessando o nome...  Alice

# Modificando o atributo nome (chama o método nome.setter)
pessoa1.nome = "Bob" # Saída: Alterando o nome...

# Acessando o atributo idade (chama o método idade())
print(pessoa1.idade) # Saída: Acessando a idade...  30

# Modificando o atributo idade (chama o método idade.setter)
pessoa1.idade = 35 # Saída: Alterando a idade...
```

Nesse exemplo em python, quando acessamos as propriedades `nome` ou `idade`, estamos na verdade acessando os atributos `_nome` e `_idade`.
E sempre que fazemos alguns desses acessos o programa escreve `"Acessando"` ou `"Alterando"` e o nome da propriedade.
Podemos dizer que o recurso de propriedades foi usada para realizar [logs](https://pt.wikipedia.org/wiki/Log_de_dados) do programa.

Cada linguagem tem suas próprias peculiaridades para implementar propriedades, a seguir temos o mesmo exemplo em typescript:

```typescript
class Pessoa {
  private _nome: string;
  private _idade: number;

  constructor(nome: string, idade: number) {
    this._nome = nome;
    this._idade = idade;
  }

  get nome(): string {
    console.log("Acessando o nome...");
    return this._nome;
  }

  set nome(novoNome: string) {
    console.log("Alterando o nome...");
    this._nome = novoNome;
  }

  get idade(): number {
    console.log("Acessando a idade...");
    return this._idade;
  }

  set idade(novaIdade: number) {
    console.log("Alterando a idade...");
    this._idade = novaIdade;
  }
}

// Criando um objeto da classe Pessoa
const pessoa1 = new Pessoa("Alice", 30);

// Acessando o atributo nome (chama o getter nome())
console.log(pessoa1.nome); // Saída: Acessando o nome... Alice

// Modificando o atributo nome (chama o setter nome())
pessoa1.nome = "Bob"; // Saída: Alterando o nome...

// Acessando o atributo idade (chama o getter idade())
console.log(pessoa1.idade); // Saída: Acessando a idade... 30

// Modificando o atributo idade (chama o setter idade())
pessoa1.idade = 35; // Saída: Alterando a idade...

```

Em PHP não temos a palavra reservada property, mas podemos implementar algo bem semelhantes utilizando os [método mágicos](https://www.php.net/manual/pt_BR/language.oop5.magic.php),
[__get](https://www.php.net/manual/pt_BR/language.oop5.overloading.php#object.get) e [__set](https://www.php.net/manual/pt_BR/language.oop5.overloading.php#object.set).

Em java não temos esse recurso de property, mas temos algumas ferramentas que transformam os getters e setters em algo semelhante.
Normalmente essas ferramentas são usadas para vincular dados de objetos com interfaces gráficas ou serialização.

A falta de propriedades em java é um dos motivos para usarmos tantos getters e setters,
se em algum momento precisarmos adicionar alguma ação, podemos alterar os métodos getters e setters.
Os consumidores dessa classe não vão notar a diferença, pois já usavam o métodos getters e setters invés de acessar diretamente os atributos.
Isso também deixa explicito que estamos usando um método e não um atributo,
assim, o consumidor da classe sabe que alguma ação pode estar sendo disparada.
Pode ser confuso vem um código como `a = obj.nome` e ver algo além da atribuição acontecendo.
Algumas linguagens seguem a convenção de ter os nomes de propriedades com letras maiúsculas, deixando assim mais explicita essa possibilidade.

---

{{#quiz quiz-propriedades.toml}}
