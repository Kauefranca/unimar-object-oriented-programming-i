# Getter & Setters

Em algumas linguagens temos a convenção de criar o métodos getters e/ou setters para nossas classes,
esses métodos fornecem um modo seguro de acessar os atributos.
Seguro no sentido de evitar que um programador utilize a classe de um forma não planejada.
Assim, quando queremos ler um atributo usamos seu método `get` e quando queremos escrever em um atributos usamos seu método `set`.
Geralmente quando o atributo é to do tipo booliano usamos `is` no lugar do `get`.
E para evitar o acesso direito aos atributos, usamos algum modificador de acesso como `protected` ou `private`.

Uma linguagem que tem essa convenção muito forte é o java, classes que seguem essa convenção geralmente são chamadas de [POJO](https://en.wikipedia.org/wiki/Plain_old_Java_object) ( Plain Old Java Object ).

```java

public class Pessoa {
    private String nome;
    private int idade;
    private boolean ehAdulto;

    public Pessoa(String nome, int idade, boolean ehAdulto) {
        this.nome = nome;
        this.idade = idade;
        this.ehAdulto = ehAdulto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public boolean isEhAdulto() {
        return ehAdulto;
    }

    public void setEhAdulto(boolean ehAdulto) {
        this.ehAdulto = ehAdulto;
    }
}

```

O uso dessa convenção é tão popular que temos diversas ferramentas para gerar esse código automaticamente.
Mas para mim, a melhor maneira, no caso do java, é utilizando [`lombok`](https://projectlombok.org/).

```java
import lombok.*;

@AllArgsConstructor
@Getter
@Setter
public class Pessoa {
    private String nome;
    private int idade;
    private boolean ehAdulto;
}
```

Utilizando as anotações `@AllArgsConstructor`, `@Getter` e `@Setter` todo os métodos getters, setters e o construtor com todos os atributos serão gerados em tempo de compilação.
Assim mesmo se alteramos algum atributo, não precisamos nos preocupar em reescrever código.
Outra maneira de não se preocupar muito com isso é utilizando linguagem [kotlin](https://kotlinlang.org/), normalmente o código gerado para a JVM já contem os getters and setters.

Essa convenção pode ser usada em outras linguagens, mas normalmente não se tem muitas vantagens em linguagens interpretadas.
Imagino que no momento seja difícil até de entender que vantagem temos em fazer isso com java,
mas espero que isso fiquei mais claro quando falarmos de propriedades e imutabilidade.

Em PHP temos o termo POPO (Plain Old PHP Object), em C++ POCO ( Plain Old C++ Object) e imagino que mais termos semelhantes em outras linguagens.
No caso de C++ temos um detalhe interessante na implementação, a palavra reservada `const` logo após os métodos de leitura.

```cpp
#include <string>

class Pessoa {
private:
    std::string nome;
    int idade;
    bool ehAdulto;

public:
    Pessoa(std::string nome, int idade, bool ehAdulto) :
        nome(nome),
        idade(idade),
        ehAdulto(ehAdulto) {}

    std::string getNome() const {
        return nome;
    }

    void setNome(std::string nome) {
        this->nome = nome;
    }

    int getIdade() const {
        return idade;
    }

    void setIdade(int idade) {
        this->idade = idade;
    }

    bool isEhAdulto() const {
        return ehAdulto;
    }

    void setEhAdulto(bool ehAdulto) {
        this->ehAdulto = ehAdulto;
    }
};
```

A palavra reservada `const` serve para deixar explicito ao compilador que esse método não modifica o estado atual do objeto,
ou seja, é um método somente de leitura.
Assim o compilador tem informações o suficiente para verificar o uso do método, permitindo o uso desses métodos em variáveis `const` / imutáveis.
Qualquer método sem o `const` não pode ser usado quando a variável é `const` e métodos com `const` também não podem alterar atributos internos.
