# Enum

```java
public enum Day {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY 
}
```

<https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html>

```python
class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3
```

<https://docs.python.org/3/library/enum.html>

```php
<?php
enum Suit
{
    case Hearts;
    case Diamonds;
    case Clubs;
    case Spades;
}

```

<https://www.php.net/manual/en/language.types.enumerations.php>

```cpp
enum class Color {
    red,
    green = 20,
    blue
};
```

<https://en.cppreference.com/w/cpp/language/enum>
