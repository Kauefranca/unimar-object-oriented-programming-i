from dataclasses import dataclass


@dataclass
class Retangulo:
    largura: float
    comprimento: float

    def calcularArea(self):
        return self.largura * self.comprimento

    @staticmethod
    def from_input():
        largura = float(input('Digite a largura:\n'))
        comprimento = float(input('Digite o comprimento:\n'))
        return Retangulo(largura, comprimento)


a = Retangulo.from_input()
print(a)
print(a.calcularArea())
