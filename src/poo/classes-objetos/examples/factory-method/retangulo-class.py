from dataclasses import dataclass


@dataclass
class Retangulo:
    largura: float
    comprimento: float

    def calcularArea(self):
        return self.largura * self.comprimento

    @classmethod
    def from_input(cls):
        largura = float(input('Digite a largura:\n'))
        comprimento = float(input('Digite o comprimento:\n'))
        return cls(largura, comprimento)


a = Retangulo.from_input()
print(a)
print(a.calcularArea())
