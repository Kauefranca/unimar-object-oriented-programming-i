public class Main {

  private String name;

  private static Main INSTANCE = new Main("static");

  Main(String name) {
    this.name = name;
    System.out.println("Construtor: " + name);
  }

  protected void finalize() throws Throwable {
    System.out.println("Destrutor: " + name);
  }

  public static void main(String... args) {
    System.out.println("Hello, World!!!");
    Main a = new Main("a");
    Main b = new Main("b");
    a = null;
    System.gc();
    b = null;
  }
}
