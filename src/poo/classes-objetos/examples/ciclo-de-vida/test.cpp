#include <iostream>

class Main {
private:
  const char *const name;

public:
  Main(const char *const name) : name(name) {
    std::cout << "Construtor: " << this->name << std::endl;
  }

  ~Main() { std::cout << "Destrutor: " << this->name << std::endl; }
};

Main global_main("global");

int main(int argc, char **argv) {
  std::cout << "Hello, World" << std::endl;

  Main *c = new Main("c");
  delete c;

  Main a("a");
  Main b("b");
}
