<?php


class Main
{

    public string $name;

    function __construct(string $name)
    {
        $this->name = $name;
        echo "Construtor: ", $this->name, "\n";
    }

    function __destruct()
    {
        echo "Destrutor: ", $this->name, "\n";
    }
}

$a = new Main("a");
$a = null;
echo "Hello, World!!!", "\n";
//gc_collect_cycles();
$b = new Main("b");
