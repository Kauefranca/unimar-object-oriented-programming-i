import gc

class Main:

    def __init__(self, name: str):
        self.name = name
        print(f'Construtor: {name}')

    def __del__(self):
        print(f'Destrutor: {self.name}')


if __name__ == '__main__':
    a = Main('a')
    b = Main('b')
    a = None
    # gc.collect()
    print('Hello, World!!!')
