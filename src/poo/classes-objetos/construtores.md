
# Método Construtor

O método construtor é responsável por inicializar o objeto que estamos construindo, cada linguagem pode ter um padrão para o nome desse método.
Em java e C++ o método tem o mesmo nome que a classe, em python usamos [`__init__`](https://docs.python.org/3/reference/datamodel.html?highlight=__init__#object.__init__),
em php usamos [`__construct`](https://www.php.net/manual/en/language.oop5.decon.php), mas a ideia é sempre a mesma, um método que recebe os parâmetros para inicializar o objeto.

Nos exemplos anteriores utilizamos o `@dataclass` e o `@AllArgsConstructor`,
ambos são utilizados para geração automática de código,
inclusive do método construtor e por isso o omitimos,
mas sempre precisamos de um método construtor.
Dependendo da classe e da linguagem de programação o método construtor pode ser gerado automaticamente dando a impressão de que ele não existe.

Nos exemplos seguintes o método construtor foi feito da maneira mais simples possível, e basicamente igual ao gerado automático pelo `@dataclass` e o `@AllArgsConstructor`.

## Python

```python
class Retangulo:

    largura: float
    comprimento: float

    def __init__(self, largura: float, comprimento: float):
        self.largura = largura
        self.comprimento = comprimento


if __name__ == '__main__':
    # criando novo objeto
    a = Retangulo(10, 15)
        
```

## Java

```java
class Retangulo {

    public float largura;
    public float comprimento;

    public Retangulo(float largura, float comprimento) {
        this.largura = largura;
        this.comprimento = comprimento;
    }

    public static void main(String... args) {
        // criando novo objeto
        Retangulo a = new Retangulo(10, 15);
    }
}
```

## PHP

```php
class Retangulo {

    public float $largura;
    public float $comprimento;

    function __construct(float $largura, float $comprimento) {
        $this->largura = $largura;
        $this->comprimento = $comprimento;
    }
}

if (!debug_backtrace()) {
    // criando novo objeto
    $a = new Retangulo(10, 15);
}
```

Com PHP podemos definir os atributos usando os parâmetros do método construtor apenas adicionando o modificador de acesso.
Em typescript temos uma sintaxe bem semelhante.

```php
class Retangulo {

    function __construct(
        public float $largura,
        public float $comprimento
    ) {
    }
}

if (!debug_backtrace()) {
    // criando novo objeto
    $a = new Retangulo(10, 15);
}
```

## C++

```cpp
class Retangulo {
public:
    float largura;
    float comprimento;

    Retangulo(float largura, float comprimento) {
        this->largura = largura;
        this->comprimento = comprimento;
    }
}

int main(int argc, char** argv) {
    // criando novo objeto
    Retangulo a(10, 15);
}

```

```cpp
class Retangulo {
public:
    float largura;
    float comprimento;

    Retangulo(float largura, float comprimento) :
        largura(largura),
        comprimento(comprimento) {
    }
}

int main(int argc, char** argv) {
    // criando novo objeto
    Retangulo a(10, 15);
}

```

Para criarmos um novo objeto sempre precisamos fornecer os parâmetros do método construtor.

## Construtor Padrão / Default

No código em C a seguir estamos criando uma variável `valor` do tipo `int`, mas não atribuímos um valor inicial para ela.
Desse modo o valor inicial se torna "aleatório", a memoria foi reservada para a variável e seu valor inicial é apenas a "sujeira" que ainda estava no mesmo endereço.

```c
<!-- cmdrun cat examples/construtor/int-sem-init.c -->
```

```console
<!-- ocirun gcc cd examples/construtor/ && gcc -x c++ int-sem-init.c -lstdc++ -o test.run && ./test.run  -->
```

Para evitar esse comportamento que muitas vezes pode levar a erros inicializamos a variável com o valor que desejamos.
No caso do `int` geralmente é algo simples como um `int valor = 0`,
mas podemos ter casos bem mais complexos quando estamos falando de objetos.

---

Como dica, mantenha o método construtor simples, ele deve no máximo fornecer alguns valores padrões e realizar pequenas conversões.
Caso seu método construtor comece a ficar complexo, talvez esteja na hora de usar `factory methods`, ou `builders`.

---

## Dúvidas frequentes

- O que é o `self` no python?

    > Em Python, o self é uma convenção utilizada dentro de uma classe para se referir ao próprio objeto criado a partir dessa classe.
    > Ele é um parâmetro implícito que indica que um método pertence à instância da classe em que está sendo utilizado.
    >
    > Ao chamar um método em um objeto específico da classe, o Python automaticamente passa a própria instância como o primeiro argumento para o método, usando o parâmetro self. Isso permite que o método acesse e trabalhe com os  dados específicos daquela instância.
    >
    > Em resumo, o self é uma convenção em Python usada como o primeiro parâmetro em métodos de classe, permitindo que o método acesse os atributos e métodos da instância em que está sendo chamado. Ele representa o próprio objeto criado a partir da classe e é uma parte importante da programação orientada a objetos em Python.
    > [^chat-gpt]

    É o equivalente ao `this` em várias outras linguagens.
