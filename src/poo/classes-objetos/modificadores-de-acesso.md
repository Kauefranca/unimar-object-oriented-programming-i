# Modificadores de Acesso

## public

## private

## Java

- public
- protected
- private
- default

## C++

- public
- protected
- private

- [friend](https://cplusplus.com/doc/tutorial/inheritance/)

# Python

- _
- __
