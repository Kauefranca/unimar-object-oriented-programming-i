# Agradecimentos

[MdBook](https://github.com/rust-lang/mdBook)  
[Sytse Reitsma - PlantUML MdBook Preprocessor](https://github.com/sytsereitsma/mdbook-plantuml)  
[Joseph Priou -  CmdRun MdBook Preprocessor](https://github.com/FauconFan/mdbook-cmdrun)  
[Cognitive Engineering Lab - Quiz MdBook Preprocessor](https://github.com/cognitive-engineering-lab/mdbook-quiz)
