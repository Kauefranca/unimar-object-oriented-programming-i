# Projeto Integrador

O tema dos seus projetos integradores serão definidos por vocês,
os professores vão apenas orientar e sugerir mudanças para atender o requisitos.

Os requisitos gerais serão abordados mais profundamente nas aulas de "Fabrica de Projetos",
mas tenha em mente que o projeto precisa integrar todas as disciplinas do semestre.

A avaliação da disciplina de POO será baseada no Projeto Integrador,
no decorrer das aulas vocês precisam se certificar que o projeto está atendendo os requisitos
listados abaixo.
No final do semestre espera-se que todos, ou pelo menos a maioria, tenham sido atendidos.

Cada aluno, **individualmente**, precisa entregar um relatório justificando como atendeu cada requisito.
Os projetos são em grupo, mas cada aluno tem que elaborar suas próprias justificativas.
Justificativas muito genéricas serão desconsideradas, procure ser especifico,
citar exemplos e anexar artefatos que comprovam a entrega.
Também serão desconsideradas justificativas iguais,
ou seja, não copie e cole de seus colegas.

A nota de P1 inclui as entregas até a data da P1 mais o bonus de frequência do bimestre,
a nota de P2 inclui as demais entregas mais o bonus de frequência do bimestre.

## Conceitos Básicos - 10 Pontos

- [ ] Codificou classes
- [ ] Codificou atributos
- [ ] Codificou métodos
- [ ] Codificou atributos estáticos
- [ ] Codificou métodos estáticos
- [ ] Codificou métodos construtores
- [ ] Codificou métodos destrutores [^c++]
- [ ] Codificou atributos protegidos e/ou privados [^c++] [^java]
- [ ] Codificou métodos protegidos e/ou privados [^c++] [^java]
- [ ] Codificou interfaces ou classes puramente virtuais
- [ ] Codificou classes abstratas ou classes virtuais
- [ ] Instanciou objetos
- [ ] Instalou e usou bibliotecas de terceiros [^java] [^python]
- [ ] Codificou enums
- [ ] Codificou propriedades [^python]

## Design - 10 Pontos

- [ ] Identificou e codificou classes de dados
- [ ] Identificou e codificou classes de comportamento
- [ ] Usou polimorfismo
- [ ] Usou objetos imutáveis
- [ ] Usou diagramas UML para discutir a solução
- [ ] Ocultou informações usando atributos e ou métodos protected/private [^c++] [^java]
- [ ] Ocultou informações usando interfaces ou classes puramente virtuais
- [ ] Codificou classes imutáveis

## Boas Práticas - 10 Pontos

- [ ] Codificou testes unitários
- [ ] Codificou padrões de projeto
- [ ] Usou conceitos de SOLID
- [ ] Usou conceitos de código limpo

## Extras - 1 Ponto cada

- [ ] Versionou todo o projeto integrador com GIT
- [ ] Publicou todo projeto integrador no Gitlab, Github, ou semelhantes
- [ ] Implantou/Hospedou ( deploy ) o projeto integrador
- [ ] Contribuiu com o material da disciplina criando "issues"
- [ ] Contribuiu com o material da disciplina criando PRs/MRs
- [ ] Publicou pacotes/bibliotecas do projeto integrador no pypi, maven central, ou semelhantes

---

[^c++] Projetos em C++ ou similares

[^java] Projetos em Java ou similares

[^python] Projetos em Python ou similares
