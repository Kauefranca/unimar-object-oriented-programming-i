# Expressões

--- Explique para um adolescente o que são expressões em programação

> Claro! Vamos explicar para um adolescente:
>
> Em programação, uma expressão é um conjunto de instruções que usamos para fazer cálculos ou executar ações. É como uma fórmula matemática que diz ao computador o que ele deve fazer.
>
> Por exemplo, vamos supor que queremos calcular a média de duas notas. A expressão seria algo como: (nota1 + nota2) / 2. Nessa expressão, estamos dizendo ao computador para somar as duas notas e depois dividir o resultado por 2.
>
> As expressões podem ser simples, como as que usamos em matemática, ou mais complexas, envolvendo várias operações e funções.
>
> Elas são muito úteis porque permitem que o computador resolva problemas e execute tarefas de forma rápida e precisa. Podemos usar expressões para fazer desde cálculos matemáticos até tomar decisões lógicas, como perguntar ao computador se uma condição é verdadeira ou falsa.
>
> Em resumo, as expressões em programação são como comandos que usamos para dar instruções ao computador e fazer coisas incríveis acontecerem! Elas são a linguagem que nos permite interagir e ensinar o computador a realizar diversas tarefas, desde cálculos simples até a criação de programas complexos.
> [^chat-gpt]

---

Assim como na matemática, as operações também seguem uma [ordem de execução ou ordem de precedência](https://pt.wikipedia.org/wiki/Ordem_de_opera%C3%A7%C3%B5es).
A ordem pode variar de linguagem para linguagem, mas todas tendem a seguir a mesma ordem da matemática,
com exceção que não usamos `{}` e `[]` para fazer agrupamento nas expressões, apenas `()`.

A ordem matemática é, potenciação e radiciação, multiplicação e divisão, e finalmente, soma e subtração.
Para deixar claro, não é que potenciação acontece antes da radiciação, elas devem ser realizadas ao "mesmo tempo"
 da esquerda para a direita.

Como estamos falando de programação, ainda temos mais operações, como funções, bit-a-bit , relacionais/comparadores e operações lógicas/boolianas.
Assim a ordem padrão é, funções, potenciação e radiciação, multiplicação e divisão, soma e subtração,
operações bit-a-bit, relacionais/comparadores ,e finalmente, operações boolianas.
Lembre-se que dentro de operações bit-a-bit e operações boolianas também temos ordem de precedência.
Nesse caso primeiro fazemos a lógica `e`, geralmente comparada a multiplicação, depois a lógica `ou`, geralmente comparada a soma.
Meu conselho ao escrever é, na dúvida adicione `()`, assim, fica claro para o leitor o que você quer fazer primeiro.
E ao ler, execute o trecho de código na linguagem desejada e observe o comportamento.

[Wiki Expressão em Computação](https://pt.wikipedia.org/wiki/Express%C3%A3o_(computa%C3%A7%C3%A3o))

## Lógica Booliana em Expressões

O uso de operações lógicas em expressões pode substituir o uso de estruturas de controle como o `if`.
É uma técnica muito utilizada em linguagens lógicas, como prolog, e em scripts como shell/bash.

Quando temos uma combinação lógica utilizando `e`, apenas um `false` faz com que o resultado se torne `false`.
Assim, o computador ao detectar o primeiro `false` ignora o restante da combinação, pois já sabe o resultado final.

```shell
operacao1 && operacao2
```

Neste exemplo, a `operacao2` só é executada se a `operacao1` resultar em `true`.
Em shell/bash, seria como executar a `operacao2` somente se a `operacao1` for bem sucedida.

Com a combinação lógica `ou` temos o comportamento inverso, ao detectar o primeiro `true`,
sabemos que o resultado final vai ser `true`, assim ignorando o restante das operações.

```shell
operacao1 || operacao2
```

Neste exemplo, a `operacao2` só é executada se a `operação1` resultar em `false`.
Em shell/bash, seria como executar a `operacao2` somente se a `operacao1` falhar.

Em python usamos `or` no lugar de `||` e `and` no lugar de `&&`, mas temos os mesmos resultados.
O `or` é muito utilizado para fornecer valores padrões, por exemplo:

```python
a = None
a = a or 2
```

Nesse exemplo, `a` foi iniciado com `None`, equivalente a `null` em outras linguagens, que significa vazio.
Ao fazermos `a or 2`, o python converte temporariamente `None` e `2` para boolianos, e assim conseguir realizar a operação `or`,
ficando `False or True`, resultando em `True`, porém, esse `True` veio da conversão temporária de `2`, assim o resultado é `2`.
A variável `a` recebe seu próprio valor ou `2`, como seu próprio valor é vazio, ela recebe `2`.
Infelizmente não temos esse comportamento em Java e C++, em Java teríamos um erro de compilação e em C++ o resultado seria `1`  que representa o `True`.

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Exercícios de fixação

{{#quiz ./quiz-expressoes.toml}}

---

## Dúvidas frequentes
