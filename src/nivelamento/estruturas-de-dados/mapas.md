# Mapas

— Explique para uma criança o que são dicionários/mapas em programação

> Claro! Vou explicar o que são dicionários ou mapas em programação de uma forma simples.
>
> Vamos imaginar que você tem um livro de contos de fadas. Nesse livro, cada história tem um título e você pode encontrar as histórias facilmente olhando o título correspondente.
>
> Em programação, um dicionário ou mapa é como esse livro de contos de fadas. É uma estrutura de dados que nos permite associar informações entre si. Funciona como um grande "livro" onde podemos encontrar informações usando chaves.
>
> Em vez de procurar por títulos de histórias, em um dicionário ou mapa em programação, usamos chaves para acessar informações relacionadas. Por exemplo, podemos ter um dicionário de animais e seus sons:
>
> "cachorro" está associado ao som "au au"
> "gato" está associado ao som "miau"
> "pato" está associado ao som "quack"
> Para encontrar o som de um animal específico, basta olhar no dicionário usando a chave correta. Por exemplo, se procurarmos pelo som do "gato", encontraremos "miau".
>
> Cada par de chave e valor em um dicionário é chamado de "entrada". É como uma página no livro que relaciona um título a uma história.
>
> Podemos adicionar novas entradas ao dicionário, remover entradas existentes ou até mesmo modificar os valores associados às chaves. É uma forma de organizar e acessar informações de maneira eficiente em um programa.
>
> Os dicionários ou mapas são muito úteis quando precisamos associar informações relacionadas ou quando queremos buscar informações rapidamente usando chaves específicas.
>
> Então, resumindo, um dicionário ou mapa em programação é uma estrutura de dados que nos permite associar informações usando chaves. É como um livro de contos de fadas, onde podemos encontrar histórias usando os títulos correspondentes. Cada entrada no dicionário é composta por uma chave e um valor associado. Eles nos ajudam a organizar e acessar informações de maneira fácil e eficiente em um programa.
> [^chat-gpt]

---

De certa forma, os mapas se assemelham com as listas,
as implementações já tratam diversos problemas que não precisamos nos preocupar,
e armazenam vários valores que acessamos com algum índice.
No caso os índices podem ser qualquer valor, desde que,
seja possível comparar e extrair um `hash`.
Na maioria daz vezes são usadas strings como chave,
mas não se limite com isso.

Os mapas recebem nomes diferentes em algumas linguagens,
em Python os chamamos de `dict` ( dicionários ),
em javascript de `object` ( objetos ) e
em Java de `Map`.

É um recurso muito utilizados em formatos de dados, como json, yaml e toml.

Exemplo de YAML:

```yaml
name: 'Fulano'
birthday: '1995-05-26' 
```

Exemplo de JSON:

```json
{
    "name" : "Fulano",
    "birthday": "1995-05-26"
}
```

Exemplo de TOML:

```toml
name = "Fulano"
birthday = "1995-05-26"
```

Todos esses exemplos estão representado os mesmos dados,
o mesmo mapa.

| Índice   |  Valor     |
|----------|------------|
| name     |  Fulano    |
| birthday | 1995-05-26 |

## Python

Em python usamos `{}` ou `dict()` para criar um novo dicionário vazio.
Podemos também iniciar com valores predefinidos,
a sintaxe com `{}` fica muito semelhante com json, ou javascript.

```python
meu_dicionario = {
    "name" : "Fulano",
    "birthday": "1995-05-26",
}
```

Para acessarmos os valores seguimos os mesmos padrões que usamos com listas.

```python
meu_dicionario["name"] = "Ciclano"
print("Ciclano nasceu dia", meu_dicionario["birthday"])
```

## Java

Em java as coisas começam a complicar um pouco,
assim como com listas,
usamos uma interface com tipos genéricos
e uma classe concreta que implementa a interface.
Vamos retomar esses conceitos quando começarmos a falar
de orientação a objetos.

```java
import java.util.Map;
import java.util.HashMap;

public class Main {
    public static void main(String... args) {
        Map<String, String> meuMapa = new HashMap<>();
        meuMapa.put("name", "Fulano");
        meuMapa.put("birthday", "1995-05-25");
        System.out.println(meuMapa.get("name") + " nasceu dia " + meuMapa.get("birthday"));
    }
}
```

---

[^chat-gpt]: <https://chat.openai.com/>

JSON vs. YAML vs. TOML  
Martin Ueding  
Disponível em: <https://martin-ueding.de/posts/json-vs-yaml-vs-toml/>

---

<!--
## Exercícios de fixação

---

## Dúvidas frequentes
-->