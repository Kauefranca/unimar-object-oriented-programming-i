#include <stdio.h>

const unsigned int total_alunos = 10;
float notas[total_alunos];

int main(int argc, char **argv) {

  for (int indice = 0; indice < total_alunos; ++indice) {
    float nota;
    printf("Digite a nota do aluno %d:\n", indice);
    scanf("%f", &nota);
    notas[indice] = nota;
  }

  float media = 0;
  for (int indice = 0; indice < total_alunos; ++indice) {
    media += notas[indice];
  }
  media /= total_alunos;

  printf("Media: %f\n", media);

  for (int indice = 0; indice < total_alunos; ++indice) {
    if (notas[indice] < media) {
      printf("aluno %d abaixo da media\n", indice);
    }
  }

  return 0;
}
