# Listas

--- Explique para uma criança o que são listas em programação

> Claro! Vou explicar o que são listas em programação de uma forma simples.
>
> Imagine que você tem uma lista de compras. Nessa lista, você anota todos os itens que precisa comprar no supermercado. Por exemplo, você pode ter na lista: leite, pão, bananas e biscoitos.
>
> Em programação, uma lista é como essa lista de compras. É uma coleção de elementos, onde cada elemento tem uma posição específica na lista. Podemos usar listas para armazenar diferentes tipos de informações, como números, palavras ou até mesmo outras listas.
>
> Vamos imaginar que estamos fazendo um programa para armazenar nomes de amigos. Podemos usar uma lista para guardar esses nomes. Por exemplo, podemos ter uma lista de amigos com os nomes: Maria, João, Ana e Pedro.
>
> Cada nome nessa lista tem uma posição, também chamada de índice. É como se cada nome tivesse um número que representa sua posição na lista. Por exemplo, Maria está na posição 0, João está na posição 1, Ana está na posição 2 e Pedro está na posição 3.
>
> Podemos acessar os elementos da lista usando o seu índice. Por exemplo, se quisermos saber o nome que está na posição 2, podemos olhar na lista e ver que é "Ana".
>
> Além disso, podemos adicionar novos elementos à lista, remover elementos existentes ou até mesmo modificar elementos. Por exemplo, se quisermos adicionar o nome "Carlos" à lista de amigos, podemos colocá-lo na próxima posição disponível.
>
> As listas são muito úteis porque nos permitem armazenar várias informações de forma organizada e acessá-las quando precisamos. Podemos percorrer a lista, fazer pesquisas ou realizar outras operações com os elementos.
>
> Então, resumindo, uma lista em programação é uma coleção de elementos, onde cada elemento tem uma posição específica na lista. Podemos usar listas para armazenar diferentes tipos de informações. Elas nos ajudam a organizar e acessar as informações de maneira fácil em um programa. É como uma lista de compras, onde podemos anotar os itens que precisamos comprar e acessá-los quando vamos ao supermercado.
> [^chat-gpt]

---

Listas são tipos especiais,
também chamados de classes ou objetos,
para armazenar vários valores de forma ordenada.
Cominados com estruturas de repetição se tornam muito úteis para deixar nosso código mais dinâmico.

Antes de aprender listas tendemos a criar uma variável para cada coisa,
então, se o problema envolver calcular a media de notas de 10 alunos,
logo pensamos em 10 variáveis, como `aluno1`, `aluno2`, ..., `aluno10`.
Como eu já conheço lista já fico com preguiça só de imaginar, mas vamos seguir com um exemplo:

```python
<!-- cmdrun cat examples/media-sem-lista.py -->
```

Se fornecemos as seguintes entradas:

```console
<!-- cmdrun cat examples/media-in.txt -->
```

Teremos esse resultado:

```console
<!-- ocirun python python examples/media-sem-lista.py < examples/media-in.txt -->
```

Usei 10 alunos para deixar claro o quanto é custoso e tedioso escrever um código assim,
além disso é péssimo para manutenção.
É fácil demais errar um número e difícil de modificar para aceitar mais on menos alunos.

Agora usando uma lista, poderíamos armazenar as 10 notas em apenas um variável.
Mas para isso funcionar, precisamos usar algum tipo de identificador para saber qual das 10 notas queremos usar da variável, chamamos esse identificador de índice.
Podemos imaginar que essa variável se comporta como uma coluna
de uma tabela e  usamos o índice para identificar de qual linha estamos falando.

| Índice/linha | Valor   |
|--------------|---------|
| 0            |  10     |
| 1            |  9      |
| 2            |  8      |
| 3            |  8      |
| 4            |  1      |
| 5            |  6      |
| 6            |  7      |
| 7            |  5      |
| 8            |  8      |
| 9            |  9      |

O formato da estrutura `for` em python oculta um pouco o índice, por isso farei primeiro exemplos em C/C++ e Java.

## C/C++ com Vetor

```cpp
<!-- cmdrun cat examples/media-com-lista.cpp -->
```

Se fornecemos as seguintes entradas:

```console
<!-- cmdrun cat examples/media-in.txt -->
```

Teremos esse resultado:

```console
<!-- ocirun gcc gcc -x c++ -lstdc++ examples/media-com-lista.cpp -o test.run && ./test.run < examples/media-in.txt -->
```

Nesse exemplo, temos a variável `notas`, o tipo dela é ponteiro de float, `float*`.
Em C/C++ podemos alocar espaços de memoria de forma sucessiva,
como colocamos `notas[total_alunos]` com `total_alunos=10`,
foi reservado espaço para 10 `float`s consecutivos.
O ponteiro aponta para o primeiro `float` nesse endereço, assim quando colocamos `nota[0]`,
ele mantém o mesmo endereço, como uma soma do endereço inicial com `0`.
Quando colocamos algum outro número,
esse ponteiro é deslocado de acordo com o tamanho do tipo da variável,
basicamente uma soma do endereço inicial com o produto do índice e do tamanho do tipo.
Ou seja, `nota[1]` é basicamente o mesmo que `nota + 1 * sizeof(float)`,
e `nota[2]` basicamente o mesmo que `nota + 2 * sizeof(float)`.

Se você não entender essa idea de memoria profundamente, não tem problema, geralmente só "escovadores de bits" pensam nisso.
O mais importante é entender que o índice, valor entre `[]` seleciona qual valor estamos usando desse vetor.
Na maior parte das linguagens o índice inicia em `0` e acaba em `tamanho -1`, no caso dos `10` alunos,
começa em `0` e termina em `9`.

## Java com Vetor

```java
<!-- cmdrun cat examples/media-com-array.java -->
```

Se fornecemos as seguintes entradas:

```console
<!-- cmdrun cat examples/media-in.txt -->
```

Teremos esse resultado:

```console
<!-- ocirun openjdk:11 java examples/media-com-array.java  < examples/media-in.txt -->
```

## Vetores vs Listas

Geralmente quando estamos falando de vetores,
estamos alocando memoria de forma linear,
um bloco continuo do tamanho que precisarmos.
Mas temos um problema,
se o vetor precisar crescer provavelmente os endereços seguintes não vão estar livres,
e vamos precisar alocar um bloco maior em outro lugar e copiar todos o itens antigos para lá.

As listas geralmente já tem alguma implementação para resolver esse problema,
elas podem alocar esse novos blocos, ou fazer conexões dinâmicas  e etc.
Nós como usuários dessas listas só nos preocupamos e inserir, ler e remover itens.

## Python

Em python não conseguimos declarar vetores como em Java e C++,
por padrão utilizamos uma implementação de lista `list`.
Podemos iniciar uma lista vazia usando `[]`, ou `list()`.
As listas tem funções próprias assim como as listas em java,
mas também podemos usar índices diretamente com `[]`.
Geralmente não é "pythonico" utilizar índices,
e damos preferencia para o uso do `for in` e do `enumerate`.

```python
<!-- cmdrun cat examples/media-com-lista.py -->
```

Se fornecemos as seguintes entradas:

```console
<!-- cmdrun cat examples/media-in.txt -->
```

Teremos esse resultado:

```console
<!-- ocirun python python examples/media-com-lista.py < examples/media-in.txt -->
```

## Java com Lista

Em java as coisas começar a complicar um pouco,
usamos uma interface com tipagem genérica `List`
e uma implementação concreta `ArrayList`.
Vamos voltar nesse assuntos quando começarmos a falar
de programação orientada a objetos.

```java
<!-- cmdrun cat examples/media-com-lista.java -->
```

Se fornecemos as seguintes entradas:

```console
<!-- cmdrun cat examples/media-in.txt -->
```

Teremos esse resultado:

```console
<!-- ocirun openjdk:11 java examples/media-com-lista.java  < examples/media-in.txt -->
```

---

[^chat-gpt]: <https://chat.openai.com/>

---

<!--
## Exercícios de fixação

---

## Dúvidas frequentes

-->