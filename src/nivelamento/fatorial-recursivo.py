def fatorial(a: int) -> int:
    if a == 0:
        return 1
    return a * fatorial( a - 1)

f = fatorial(10)
print(f)
