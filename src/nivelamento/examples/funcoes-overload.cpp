#include <iostream>

int soma(int a, int b) {
  std::cout << "Soma int: " << a << " " << b << std::endl;
  return a + b;
}

float soma(float a, float b) {
  std::cout << "Soma float: " << a << " " << b << std::endl;
  return a + b;
}

int main(int argc, char **argv) {
  int i1 = 2;
  int i2 = 2;
  float f1 = 2;
  float f2 = 2;
  int i3 = soma(i1, i2);
  float f3 = soma(f1, f2);

  std::cout << i3 << std::endl;
  std::cout << f3 << std::endl;
}
