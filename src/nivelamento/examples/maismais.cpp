#include <iostream>

int main(int argc, char** argv) {
    int a = 0;
    int b = ++a;
    int c = a++;

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "c = " << c << std::endl;
}
