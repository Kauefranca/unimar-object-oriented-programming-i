#include <stdio.h>

int main(int argc, char** argv) {
    int tabuada_do = 7;
    for(int fator = 1; fator < 11; ++fator) {
        printf("%d x %d = %d\n", fator, tabuada_do, fator * tabuada_do);
    }
}
