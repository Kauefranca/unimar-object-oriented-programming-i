def create_add(to_add):
    def add(value):
        return value + to_add
    return add


add_2 = create_add(2)

print(add_2(1))

print(create_add(10)(12))
