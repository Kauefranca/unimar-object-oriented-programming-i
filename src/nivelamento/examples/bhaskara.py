def bhaskara(a, b, c):
    delta = b ** 2 - 4 * a * c
    if delta < 0:
        raise Exception("Números negativos não tem raiz quadrada")
    return (-b + delta ** 0.5) / (2 * a), (-b - delta ** 0.5) / (2 * a)


bhaskara(3, 2, 2)
