#include <array>
#include <iostream>
#include <math.h>

using namespace std;

array<float, 2> bhaskara(float a, float b, float c) {
  float delta = powf32(b, 2) - 4 * a * c;
  if (delta < 0) {
    throw "Números negativos não tem raiz quadrada";
  }
  return array<float, 2>{
      (-b + sqrtf32(delta)) / (2 * a),
      (-b - sqrtf32(delta)) / (2 * a),
  };
}

int main(int argc, char **argv) {
  try {
    bhaskara(3, 2, 2);
  } catch (char const *mensagem) {
    cout << mensagem << endl;
  }
  bhaskara(3, 2, 2);
}
