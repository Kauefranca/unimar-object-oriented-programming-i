try:
    a = float(input())
    b = float(input())
    c = a / b
except ValueError as error:
    print('Parece que você digitou algo errado')
    print(error)
except ZeroDivisionError as error:
    print('Não podemos dividir por zero')
    print(error)
