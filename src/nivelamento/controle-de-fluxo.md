# Controle de Fluxo

--- Explique para uma criança o que é controle de fluxo em programação

> Claro! Vou explicar o que é controle de fluxo em programação de uma forma simples.
>
> Imagine que você está brincando de seguir instruções em uma brincadeira. Alguém diz "Pule duas vezes" e você pula duas vezes. Depois, outra pessoa diz "Dê uma volta" e você dá uma volta. Essas instruções estão controlando o seu fluxo de movimento na brincadeira.
>
> Em programação, o controle de fluxo é como as instruções que direcionam o caminho que o programa segue. É como um conjunto de comandos que dizem ao programa o que fazer em diferentes situações.
>
> Por exemplo, podemos ter uma instrução que diz: "Se está chovendo, pegue um guarda-chuva. Caso contrário, vá para a rua sem guarda-chuva". Essa instrução controla o fluxo do programa, tomando decisões com base na condição do tempo.
>
> Existem diferentes tipos de controle de fluxo em programação. Alguns exemplos são:
>
> Condicionais: São instruções que verificam condições e executam diferentes ações com base nessas condições. É como uma bifurcação em um caminho, onde o programa pode seguir um caminho se uma condição for verdadeira ou outro caminho se for falsa.
>
> Loops: São instruções que permitem repetir um bloco de código várias vezes. É como quando você faz um movimento em uma dança e o repete várias vezes. Os loops permitem que o programa execute uma ação repetidamente até que uma condição seja atendida.
>
> Saltos: São instruções que permitem que o programa pule para uma parte específica do código. É como quando você pula uma parte de uma música e vai direto para o refrão. Os saltos ajudam a controlar o fluxo do programa de forma não linear.
>
> O controle de fluxo é importante porque permite que os programas tomem decisões, repitam tarefas e executem diferentes ações dependendo das condições. Ele nos dá o poder de criar programas dinâmicos e adaptáveis.
>
> Resumindo, o controle de fluxo em programação são as instruções que direcionam o caminho que o programa segue. É como um conjunto de comandos que dizem ao programa o que fazer em diferentes situações. Existem condicionais, loops e saltos que nos permitem tomar decisões, repetir tarefas e controlar o fluxo do programa de maneira flexível.
> [^chat-gpt]

---

Até agora só falamos de algoritmos com um único fluxo, que nunca precisamos decidir se vamos para um lado ou para o outro.
Assim como no diagrama a abaixo, do "passo 1" vamos para o "passo 2" e do "passo 2" para o "passo 3", seguimos apenas para uma única direção.

```plantuml
@startuml

start
:Passo 1;
:Passo 2;
:Passo 3;
stop

@enduml
```

Mas geralmente os problemas da vida real não são solucionados de maneira tão simples,
normalmente precisamos ramificar os caminhos que podemos percorrer
e as vezes repetir passos.
Como exemplo, considere o fluxograma abaixo:

```plantuml
@startuml

title "Fluxograma da Engenharia"

start

if (Está se movendo?) then (Sim)
  if (Deveria?) then (Sim)
    :Sem problemas;
    stop
  else ( Não )
    repeat
      :Prenda com Silvertape;
    repeat while ( Continua se movendo? ) is (Sim) not (Não)
  endif
else (Não)
  if (Deveria?) then (Sim)
    repeat
      :Aplique WD-40;
    repeat while ( Está se movendo? ) is (Não) not (Sim)
  else ( Não )
    :Sem problemas;
    stop
  endif
endif
:Parabéns, você consertou;
stop

@enduml
```

Esse fluxograma é uma brincadeira com como engenheiros consertam coisas.

## Estruturas Condicionais

### If

O `if` se traduz como `se` e o utilizamos com a mesma ideia de quando o falamos.
Se isso então isso.
Ou dando nomes, se condição então ação.
A condição é qualquer coisa que possa ser convertida para um booliano, verdadeiro ou falso,
e a ação um bloco de código.

O `if` em um fluxograma:

```plantuml
@startuml

start
:Passo 1;
if (Devo realizar o Passo 2?) then ( Sim )
    :Passo 2;
else ( Não )
endif
:Passo 3;
stop

@enduml
```

O `if` em python:

```python
print('passo1')
if passo2:
  print('passo2')
print('passo3')
```

Um exemplo um pouco mais prático:

```python
<!-- cmdrun cat examples/comprar-if.py -->
```

```python
<!-- ocirun python python examples/comprar-if.py -->
```

Nesse exemplo conferimos se temos dinheiro o suficiente para comprar algo,
comparando `preco` com `dinheiro`.
Se `dinheiro` for maior ou igual a `preco`, temos condição de comprar,
 então  compramos,
isso subtrai o `preco` da variável `dinheiro`.
Se não tivermos condição, não fazemos nada.

### If Else

Com o `else` podemos adicionar um passo alternativo,
caso a condição do `if` seja falsa.

O `if else` em um fluxograma:

```plantuml
@startuml

start
:Passo 1;
if (Devo realizar o Passo 2?) then ( Sim )
    :Passo 2;
else ( Não )
    : Passo Alternativo;
endif
:Passo 3;
stop

@enduml
```

O `if else` em python:

```python
print('passo1')
if passo2:
  print('passo2')
else:
  print('Passo alternativo')
print('passo3')
```

### If Else If

Também podemos colocar um `if` logo apó de cada `else`,
formando o `else if`, ou em python `elif`.

O `else if` em um fluxograma:

```plantuml
@startuml
start
if (Opção 1) then (Sim)
  :Executar Opção 1;
(Não) elseif (Opção 2) then (Sim)
  :Executar Opção 1;
(no) elseif (Opção 3) then (Sim)
  :Executar Opção 1;
(no) elseif (Opção 4) then (Sim)
  :Executar Opção 1;
else (Não)
  :Opção Inválida;
endif
stop
@enduml

```

O `elif` em python:

```python
opcao = int(input('Digite a opção desejada: '))
if opcao == 1:
  opcao1()
elif opcao == 2:
  opcao2()
elif opcao == 3:
  opcao3()
elif opcao == 4:
  opcao4()
else:
  print('Opção Inválida')
```

Em java, ou C++, usamos `else if`, precisamos de `{}` para delimitar os blocos de código
e `()` para delimitar as condições.

Em java:

```java
public class Main {
  public static void main(String... args) {
    Scanner scanner = new Scanner(System.in);
    int opcao = scanner.nextInt();
    if (opcao == 1) {
      opcao1();
    }
    else if (opcao == 2) {
      opcao1();
    }
    else if (opcao == 3) {
      opcao1();
    }
    else if (opcao == 4 ) {
      opcao1();
    }
    else {
      System.out.println("Opção Inválida")
    }
  }
}
```

Em C/C++:

```cpp

#include <stdio.h>

int main(int argc, char** argv) {
  int opcao = 0;
  scanf("%d", &opcao);
  if (opcao == 1) {
    opcao1();
  }
  else if (opcao == 2) {
    opcao1();
  }
  else if (opcao == 3) {
    opcao1();
  }
  else if (opcao == 4 ) {
    opcao1();
  }
  else {
   printf("Opção Inválida\n")
  }
}

```

### Switch

Temos uma estrutura própria para situações com muitas condições,
o `switch`, infelizmente, ou felizmente, essa estrutura não existe em python.
Mais adiante, em orientação a objetos, quando falarmos de polimorfismo, voltaremos nesse assunto.

Um `swicth` em um fluxograma:

```plantuml
@startuml
start
switch (Opção)
case ( 1 )
  :Executar Opção 1;
case ( 2 ) 
  :Executar Opção 2;
case ( 3 )
  :Executar Opção 3;
case ( 4 )
  :Executar Opção 4;
case ( ? )
  :Opção Inválida;
endswitch
stop
@enduml

```

<!--

### Pattern Matching/Casamento de Padrões

<https://pt.wikipedia.org/wiki/Casamento_de_padr%C3%B5es>

-->

## Estruturas de Repetição

### While

O `while` tem um funcionamento semelhante ao `if`,
mas ele repete enquanto a condição for verdadeira.

O `while` em um fluxograma:

```plantuml
@startuml
start

while (data available?) is (yes)
  :read data;
  :generate diagrams;
endwhile ( no )

stop
@enduml
```

Exemplo da tabuada do 7 usando o `while` em python:

```python
<!-- cmdrun cat examples/tabuada7-while.py -->
```

A saída do programa:

```console
<!-- ocirun python python examples/tabuada7-while.py -->
```

### Do While

O `do while` é bem semelhante ao `while`,
mas sua primeira iteração é executa incondicionalmente.
A linguagem python não tem suporte ao `do while`.

```plantuml
@startuml

start

repeat
  :read data;
  :generate diagrams;
repeat while (more data?) is (yes)
->no;
stop

@enduml

```

Exemplo de jogo de adivinhar números em Java usando `do while`:

```java
<!-- cmdrun cat examples/adivinha.java -->
```

### For

O `for` é uma maneira sucinta de fazer laços de repetição.
Na maior parte das vezes quando estamos fazendo uma repetição temos
que definir três coisas:

- Inicialização do iterador/contador
- Condição de finalização
- Incremento do iterador/contador

O `for` utiliza esses três itens:

`for( inicialização ; condição ; incremento)`

Exemplo da tabuada do 7 uasndo `for` em C/C++:

```cpp
<!-- cmdrun cat examples/tabuada7-for.cpp -->
```

Saída do programa:

```console
<!-- ocirun gcc gcc -x c++ -lstdc++  examples/tabuada7-for.cpp -o test.run && ./test.run -->
```

Em python o `for` um pouco diferente,
ele percorre "listas" de forma automática.
Então sempre precisamos de alguma "lista" para percorrer.
A função `range` faz esse trabalho,
ela gera a lista de items que o nosso `for` vai precisar.

Exemplo da tabuada do 7 usando `for` em python:

```python
<!-- cmdrun cat examples/tabuada7-for.py -->
```

```console
<!-- ocirun python python examples/tabuada7-for.py -->
```

Na verdade o python utiliza algo que chamamos de [iterável e iterador](https://pt.wikipedia.org/wiki/Iterador),
por isso podemos fazer um `for in` sem nos preocupar com um contador.
Em outras linguagens geralmente chamamos isso de `foreach`.
Nesses casos o próprio objeto que estamos percorrendo se encarrega de criar um iterável e controlar o contador.
Em Java e C++ também temos esse recurso,
mas vamos deixar isso para quando falarmos mais de classes e objetos.

### break

Dentro de qualquer laço, `while`, `do while` e `for`,
podemos usar o comando `break` para forçar a interrupção do laço.
É bem útil quando temos mais de uma condição finalizar o laço.

### continue

Dentro de qualquer laço, `while`, `do while` e `for`,
podemos usar o comando `continue` para pular a iteração atual.

### Cuidado com laços infinitos ⚠️

Alguns são fáceis de identificar:

```python
while True:
  print('Olha eu de novo')
```

Outros nem tanto:

```c++
#include <iostream>

int main(int argc, char** argv) {
  for(int=0; i > 0; ++i) {
    std::cout << "Olha eu de novo" << std::endl;
  }
}
```

```python
def fatorial(n):
  return n * fatorial(n-1)
```

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Exercícios de fixação

{{#quiz ./quiz-controle-de-fluxo.toml}}

---

## Dúvidas frequentes

- O que é o `return`?

    > O `return` é usado para indicar o valor que uma função deve retornar quando é chamada. É como uma forma de enviar de volta um resultado ou um dado específico para quem chamou a função. Ele permite que a função forneça um valor útil que pode ser usado em outras partes do programa.  
    > Além de indicar o valor de retorno, o `return` também finaliza a execução da função. Isso significa que quando a função alcança uma instrução `return`, ela é encerrada imediatamente e qualquer código subsequente não é executado. [^chat-gpt]
