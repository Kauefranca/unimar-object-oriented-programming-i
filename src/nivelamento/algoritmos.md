# Algoritmos

--- Explique para uma criança o que é um algoritmo

> Claro! Vou explicar o que é um algoritmo de uma forma simples.
>
> Um algoritmo é uma sequência de passos que nos ajuda a resolver um problema. É como uma receita que seguimos para realizar uma tarefa específica.
>
> Vamos pensar em um exemplo. Digamos que você queira fazer um sanduíche. Para fazer um sanduíche delicioso, você precisa seguir alguns passos:
>
> Pegue duas fatias de pão.
> Coloque o recheio que você gosta, como queijo, presunto, alface e tomate, entre as duas fatias de pão.
> Aperte as fatias de pão juntas.
> Pronto! Agora você tem um sanduíche gostoso para comer.
> Esses passos que você seguiu para fazer o sanduíche são um algoritmo. Eles são uma sequência de instruções que você pode repetir sempre que quiser fazer um sanduíche.
>
> Da mesma forma, em programação, usamos algoritmos para resolver problemas. Em vez de fazer um sanduíche, podemos ter um problema como "somar dois números" ou "encontrar o maior número em uma lista".
>
> Um algoritmo em programação é uma sequência de passos que nos ajuda a resolver esses problemas. São instruções que o computador pode entender e executar.
>
> Por exemplo, se queremos somar dois números, o algoritmo seria algo assim:
>
> Pegue o primeiro número.
> Pegue o segundo número.
> Some os dois números.
> O resultado é a resposta da soma.
> Quando escrevemos um algoritmo, queremos que ele seja claro e fácil de entender, tanto para nós quanto para o computador. Assim, podemos resolver problemas de maneira eficiente e obter os resultados desejados.
>
> Então, resumindo, um algoritmo é uma sequência de passos que nos ajuda a resolver um problema. É como uma receita que seguimos para realizar uma tarefa específica, seja fazer um sanduíche ou resolver um problema em programação. É uma forma de organizar as instruções de maneira lógica e obter os resultados desejados.
> [^chat-gpt]

---

Um algoritmo é uma sequencia de operações para realizar algum tipo de tarefa, dependendo de quem vai ler e/ou executar essas operações precisamos escrever de uma maneira diferente.
Para humanos geralmente podemos escrever de uma maneira menos formal, algumas vezes até podemos omitir informações, ou cometer erros, pois o leitor consegue deduzir ou corrigir algumas coisas.
Já um computador executa as operações exatamente como elas estão escritas, então omissão de informações, ou erros vão comprometer os resultados.

O importante no momento é conseguir organizar nossas ideias e escrever a sequencia de operações, já que ainda não estamos falando de linguagens de programação e sim de algoritmos.
Para escrever a sequencia de operações podemos usar linguagens textuais ou até mesmo visuais como [fluxogramas](https://pt.wikipedia.org/wiki/Fluxograma) ou [UML](https://pt.wikipedia.org/wiki/UML).

Vamos começar com um algoritmo ( receita ) simples para fazer café: [^panelinha]

1. Coloque o filtro de papel no coador e encaixe sobre uma jarra ou garrafa térmica.
1. Coloque a água num fervedor e leve ao fogo médio para aquecer.  
    Atenção: não deixe a água ferver, assim que começarem a subir as primeiras bolhinhas desligue o fogo – se a água estiver muito quente pode queimar o pó e deixar o café amargo em excesso.
1. Regue um pouco da água quente sobre o filtro sem o café – é bem pouco mesmo, apenas para aquecer o filtro e o coador e eliminar qualquer aroma indesejado do papel. Descarte a água.
1. Coloque o pó de café no filtro umedecido e regue apenas a quantidade suficiente de água quente para umedecer todo o pó.
1. Deixe hidratar por alguns segundos e despeje o restante da água em movimento circular para coar todo o pó por igual.
1. Deixe o café coar sem mexer.
1. Sirva a seguir.

Um algoritmo desses é simples para os humanos, mas para um computador geralmente precisamos deixa-lo mais formal.
Uma pessoa também geralmente já tem conhecimento do que é uma "garrafa térmica" ou um "filtro de papel", mas um computador pode ter dificuldades com esses conceitos.
Colocar o filtro de papel em um coador é muito complexo para um computador, mesmo se tratando de um robô, isso envolveria o controle de diversos motores, a leitura de vários sensores e etc.
Para os seres humanos existe uma complexidade semelhante, controlar os músculos, perceber objetos e etc , mas assim como respirar já é algo que fazemos sem esforço.

Os computadores são construídos utilizando componentes eletrônicos e projetados utilizando [algebra booliana](https://pt.wikipedia.org/wiki/%C3%81lgebra_booliana), pelo menos a maioria até hoje.
Por esse e outros motivos os computadores são bons em realizar tarefas lógicas e matemáticas, então tendemos a escrever algoritmos usando esses recursos. Segue um exemplo de algoritmo mais próximos das capacidades básicas de um computador, o calculo da velocidade.

1. Receber distância
1. Receber tempo
1. Dividir distância por tempo
1. Apresentar resultado

Ou como um fluxograma/[diagrama de atividade](https://pt.wikipedia.org/wiki/Diagrama_de_atividade).

```plantuml
@startuml

start
note left
    Indica o Inicio
end note
:Receber distância;
:Receber tempo;
:Dividir distância por tempo;
:Apresentar resultado;
stop
note left
    Indica o Fim
end note

@enduml
```

Muito mais simples computacionalmente falando.
Mas ainda estamos omitindo algumas informações, não especificamos as unidades de medida e não especificamos o que é o resultado que vamos apresentar.

1. Receber distância em metros
1. Receber tempo em segundos
1. Dividir distância por tempo e usar [quociente](https://pt.wikipedia.org/wiki/Divis%C3%A3o) como resultado
1. Apresentar resultado

Pode parecer obvio para a gente, mas geralmente não é para o computador.
Se um humano seguisse instruções de forma semelhante a um computador acredito que seria parecido com o vídeo a seguir:

<!-- markdownlint-disable-next-line MD033 -->
<iframe width="560" height="315" src="https://www.youtube.com/embed/pdhqwbUWf4U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Por fim, para transformar nosso algoritmo em um programa de computador precisamos escreve-lo de uma forma que o computador entenda,
precisamos traduzir para uma [linguagem de programação](https://pt.wikipedia.org/wiki/Linguagem_de_programa%C3%A7%C3%A3o).

Apenas como exemplo, segue o equivalente em python do algoritmo de calcular velocidade.

```python
<!-- cmdrun cat velocidade.py -->
```

Resultado caso o usuário fornecesse `10` ( distância em metros ) e `4` ( tempo em segundos) como entrada:

```console
<!-- ocirun python python velocidade.py < velocidade.in -->
```

[^chat-gpt]: <https://chat.openai.com/>

[^panelinha]: <https://panelinha.com.br/receita/cafe-coado>  

---

## Exercícios de fixação

{{#quiz ./quiz-algoritmos.toml}}

---

## Dúvidas frequentes
