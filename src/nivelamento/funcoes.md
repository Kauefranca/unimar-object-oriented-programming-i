# Funções

--- Explique para uma criança o que é função em programação

> Claro! Vou explicar o que é uma função em programação de uma forma simples.
>
> Imagine que você tem uma máquina mágica que pode fazer coisas especiais quando você aperta um botão. Por exemplo, quando você aperta o botão, a máquina pode pular, cantar uma música ou até mesmo fazer uma mágica.
>
> Em programação, uma função é como essa máquina mágica. É um bloco de código que recebe um nome e pode realizar uma tarefa específica quando é chamada. É como um conjunto de instruções que podem ser reutilizadas várias vezes.
>
> Uma função pode receber algumas informações, que chamamos de "parâmetros". Os parâmetros são como as instruções que você dá à máquina mágica antes de apertar o botão. Por exemplo, se você quer que a máquina pule, você pode dizer a altura em que ela deve pular como um parâmetro.
>
> Quando você chama uma função no programa, é como apertar o botão da máquina mágica. Você está dizendo ao computador para executar as instruções contidas na função.
>
> Por exemplo, vamos dizer que temos uma função chamada "cantar" que faz a máquina mágica cantar uma música. Se chamarmos essa função, o computador vai executar as instruções dentro dela e a máquina mágica vai cantar.
>
> Além disso, uma função pode retornar um valor de volta para o programa. É como se a máquina mágica entregasse algo para você depois de fazer sua tarefa. Por exemplo, a função "somar" pode receber dois números como parâmetros e retornar a soma deles.
>
> As funções são muito úteis porque nos permitem organizar o código e reutilizar trechos de instruções. Em vez de escrever as mesmas instruções várias vezes, podemos colocá-las em uma função e chamá-la sempre que precisarmos.
>
> Então, resumindo, uma função em programação é como uma máquina mágica que recebe um nome e realiza uma tarefa específica quando é chamada. Ela pode receber informações chamadas de parâmetros e pode retornar um valor. As funções nos ajudam a organizar e reutilizar nosso código de forma eficiente, como uma máquina que pode fazer coisas especiais quando apertamos um botão.
> [^chat-gpt]

---

Podemos encarar funções como "pequenos" algoritmos dentro de outro algoritmo,
ou como variáveis que ao invés de armazenar valores armazenam ações.
Assim, sempre que quisermos executar esse pequeno algoritmo, ou ação,
simplesmente chamamos essa função,
ao invés de escrever todos seus passos novamente.

Imagine uma situação em que precisamos escrever diversos e-mails.
Cada e-mail precisa ter destinatário, assunto e conteúdo, no final, precisamos envia-lo.
Nesse caso poderíamos ter uma função com o comportamento abaixo:

```plantuml

@startuml
title "Função Enviar e-mail"
start
:Abrir plataforma de e-mail;
:Escrever destinatário;
:Escrever assunto;
:Escrever conteúdo;
:Confirmar envio;
stop
@enduml


```

Com essa função definida podemos utiliza-la em nosso algoritmo principal:

```plantuml

@startuml
start
:Receber Pedido;
:Enviar e-mail ao cliente;
note right
    Chamada a função Enviar e-mail
endnote
:Aguardar pagamento;
:Enviar e-mail ao cliente;
note right
    Chamada a função Enviar e-mail
endnote
:Preparar produto;
:Despachar Produto;
:Enviar e-mail ao cliente;
note right
    Chamada a função Enviar e-mail
endnote
:Receber confirmação de entrega;
:Enviar e-mail ao cliente;
note right
    Chamada a função Enviar e-mail
endnote
stop
@enduml


```

Dessa forma estamos simplificando nosso algoritmo principal,
colocando os detalhes de envio de e-mail em outra parte.
Assim como fizemos com o fluxograma,
podemos criar/definir funções em nosso código
e depois reutiliza-lá em outras partes.

Geralmente as chamadas de função tem a seguinte sintaxe:

```python
nome_da_funcao()
```

Sim, os `()` são necessários, isso vai diferenciar uma chamada de função de uma simples leitura de variável.

É importante enfatizar que as funções tem seus próprios contextos,
variáveis criadas dentro das funções só existem dentro da função.
As funções também não acessam contextos externos, como variáveis do nosso programa principal, ou acessam com limitações.

## Usando Funções

### Parâmetros

Raramente precisamos executar uma função sempre da mesma forma,
normalmente precisamos fazer alguns detalhes de forma diferente.
Podemos usar os parâmetros para tornar nossas funções mais genéricas,
escolher esses detalhes no momento em que a chamamos,
assim podendo usar a mesma função de forma diferentes em partes diferentes do nosso programa.

No caso do e-mail, por exemplo, mesmo sendo o mesmo cliente o assunto e conteúdo do email precisam ser diferentes em cada etapa.
Assim seria ideal parametrizar destinatário, assunto e conteúdo.

```plantuml

@startuml
start
:Receber Pedido;
:Enviar e-mail ao cliente;
note right
    Chamada a função Enviar e-mail
    Parâmetro Destinatário: cliente@email.com
    Parâmetro Assunto: Seu pedido for recebido
    Parâmetro Conteúdo: ...
endnote
:Aguardar pagamento;
:Enviar e-mail ao cliente;
note right
    Chamada a função Enviar e-mail
    Parâmetro Destinatário: cliente@email.com
    Parâmetro Assunto: Pagamento confirmado, estamos preparando seu pacote
    Parâmetro Conteúdo: ...
endnote
:Preparar produto;
:Despachar Produto;
:Enviar e-mail ao cliente;
note right
    Chamada a função Enviar e-mail
    Parâmetro Destinatário: cliente@email.com
    Parâmetro Assunto: Pedido enviado
    Parâmetro Conteúdo: ...
endnote
:Receber confirmação de entrega;
:Enviar e-mail ao cliente;
note right
    Chamada a função Enviar e-mail
    Parâmetro Destinatário: cliente@email.com
    Parâmetro Assunto: Entrega confirmada,
    Parâmetro Conteúdo: ...
endnote
stop
@enduml


```

Os nomes dos parâmetros geralmente seguem as mesmas regras e convenções de nomes de variáveis.

#### Função `print`

A função `print` é uma das primeiras que vemos funcionando e para ela é essencial o uso de parâmetros.
Essa função escreve algo no console, essa é sua responsabilidade, mas não cabe a ela decidir o que escrever.
Se não fosse possível escolher o que a função `print` escreve, ela se tonaria quase que inútil.

Quando utilizamos a função `print` sempre adicionamos `()` e o que desejamos escrever dentro deles.
O que desejamos escrever é um parâmetro para a função `print`.
Um exemplo clássico é escrever "Hello, World!":

```python
print('Hello, World')
```

Nesse exemplo, o parâmetro para a função `print` é um literal `"Hello, World`, mas também poderíamos usar uma variável.

```python
nome = 'João'
print(nome)
```

A função `print` pode aceitar vários parâmetros, para isso só precisamos coloca-los dentro dos `()` e usar `,` para separar.

```python
nome = 'João'
print('Olá', nome)
```

### Retorno

As funções podem também fornecer resultados, assim como as operações.
A soma por exemplo, quando a aplicamos em dois números queremos o resultado dessa soma.
Se a operação de soma não nos retornasse algum resultado era seria praticamente inútil.
Algumas funções, como o `print`, não precisam de retorno,
já que o que queremos é apenas a ação de imprimir um texto.

#### Função `input`

A função `input` também é uma das primeiras que aprendemos, ela serve para interagir com o usuário no console.
Assim como a função `print`, é possível escrever algo usando um parâmetro,
mas a maior responsabilidade da função `input` é ler o que o usuário digita no terminal.
Esse é o resultado da função `input`, o que o usuário digitar.
Como a função `input` tem um resultado, geralmente a usamos do lado direito do `=`,
pois normalmente atribuímos seu resultado para uma variável.

```python
nome = input('Olá, qual é o seu nome? ')
print('Saudações,', nome)
```

Esse exemplo espera que o usuário digite algo e pressione enter,
armazena na variável `nome` o que foi digitado,
e finalmente escreve `'Saudações,` e o conteúdo de `nome`.

## Criando funções

Sempre que vamos criar uma função precisamos pensar em três coisas,
seu nome, seus parâmetros e seu resultado.
Provavelmente temos que pensar no que ela faz também,
mas espero que isso esteja explicito em seu nome

A função `print`, por exemplo, escreve/imprimi, ela precisa do parâmetro do que vai escrever e não precisa de um resultado/retorno.

Em python poderíamos escrever algo assim:

```python
def print(conteudo):
    # código que escreve o conteudo
```

A palavra reservada `def` serve exclusivamente para criação/**def**inição de funções.
Ela deve ser procedida pelo nome da função e pelos nomes dos parâmetros entre `()` separados por `,`.
Após os parâmetros temos um `:` e inciamos o corpo da função, o que ela realmente faz.
O corpo precisa ser [indentado](https://pt.wikipedia.org/wiki/Indenta%C3%A7%C3%A3o),
assim o python consegue distinguir o que faz parte ou não do corpo.

Em C/C++ poderíamos escrever algo assim:

```c
void print(char* conteudo) {
    // código que escrever o conteudo
}
```

Em C/C++ usamos `{}` para delimitar o corpo da função, a indentação é apenas uma convenção para facilitar a leitura.
Não temos a palavra `def`, aqui temos o `void`, mas ele não é somente para definir funções,
ele se refere ao tipo de retorno da função, veremos isso mais adiante.

Em PHP:

```php
<?php

function print($conteudo) {
    // código para escrever
}

```

Em javascript:

```js
function print(conteudo) {
    //código para escrever o conteudo
}
```

Em javascript e em PHP usamos a palavra `function` para definir uma função.

Cada linguagem pode ter algumas diferenças de sintaxe, mas sempre precisamos definir nome e parâmetros da função.
Também precisamos do corpo, mas ele pode ser vazio.
Nas linguagens de tipagem estática também devemos definir o tipo do retorno da função,
isso geralmente é opcional em linguagens de tipagem dinâmica.

O retorno de uma função é o resultado que ela deve devolver para quem a chamou.
Para interromper a execução da função e retornar esse valor utilizamos a palavra
reservada `return`.
Nenhuma linha após o `return`, dentro do corpo da função, é executada.

Em python por exemplo:

```python
def soma(a: int, b: int) -> int:
    return a + b
```

Nesse exemplo estamos definindo tipos para os parâmetros, com o `: int` após cada parâmetro, e também para o retorno da função, com o `-> int`.

Em C/C++ a ordem da tipagem muda um pouco, mas ainda temos os mesmo conceitos.

```cpp
int soma(int a, int b) {
    return a + b;
}
```

Repare que não temos mais o `void`, agora inciamos a definição da função com um `int`,
isso significa que essa função vai retornar um inteiro e não vazio (`void`).

### Sobrecarga

Em algumas linguagens temos a possibilidade de ter funções com o mesmo nome, mas que fazem coisas diferentes,
duas ou mais funções, mas todas com o mesmo nome.
Porém, para isso funcionar precisamos desambiguar as chamadas de função,
sem isso seria impossível para o computador saber qual função realmente executar.
As linguagens que tem esse suporte são linguagens com tipagem estática,
a desambiguação da chamada de função é feita verificando os tipos dos parâmetros.

```cpp
<!-- cmdrun cat examples/funcoes-overload.cpp -->
```

```console
<!-- ocirun gcc gcc -x c++ -lstdc++ examples/funcoes-overload.cpp -o test.run && ./test.run -->
```

### Sobrescrita

Em python as funções são tratadas como objetos, assim, quando estamos usando o nome delas estamos usando uma variável.
Por esse motivo podemos simplesmente sobrescrever essa variável, atribuir um novo valor para a variável da função,
ou simplesmente redefinir a função.

```python
<!-- cmdrun cat examples/funcoes-overwrite.py -->
```

```console
<!-- ocirun python python examples/funcoes-overwrite.py -->
```

## Introdução a Recursividade

Funções recursivas são funções que de alguma forma,
chamam elas mesmas novamente,
criando assim uma repetição.
Funções recursivas geralmente são "elegantes",
tem uma lógica simples e menor,
já que tiram proveito da [pilha de chamadas](https://pt.wikipedia.org/wiki/Pilha_de_chamada),
porém, pelo mesmo motivo,
podem acabar tendo menos desempenho,
principalmente relacionado a memória.

Ao criar funções recursivas precisamos ter bem definido quando vamos finalizar as chamadas e encerrar as repetições.
Para isso usamos estruturas de controles, que veremos mais adiante,
por esse motivo só estamos introduzindo a ideia.

Para os curiosos, deixarei o exemplo de uma função recursiva para calcular fatorial:

```python
<!-- cmdrun cat fatorial-recursivo.py -->
```

## Introdução a Closures

Closure é um recurso muito utilizado com programação funcional.
Com esse recurso podemos criar funções que criam funções.
As variáveis do espoco atual são atreladas com a função criada,
possibilitando a parametrização de forma diferente.

```python
<!-- cmdrun cat examples/closure.py -->
```

```console
<!-- ocirun python python examples/closure.py -->
```

[Closures in Python | Explained with animations](https://www.youtube.com/watch?v=tNSOaA1z6Uo)  
[Currying - Part 6 of Functional Programming in JavaScript](https://www.youtube.com/watch?v=iZLP4qOwY8I&t=2s)

## Introdução a lambda

Em python a palavra `lambda` é um açúcar sintático para a criação de funções,
á usamos quando vamos criar uma função de apenas uma linha.

```python
<!-- cmdrun cat examples/lambda.py -->
```

```console
<!-- ocirun python python examples/lambda.py -->
```

Em outras linguagens quando falamos de lambdas,
estamos basicamente falando de closures.

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Exercícios de fixação

{{#quiz ./quiz-funcoes.toml}}

---

## Dúvidas frequentes

- O que é o `return`?

    > O `return` é usado para indicar o valor que uma função deve retornar quando é chamada. É como uma forma de enviar de volta um resultado ou um dado específico para quem chamou a função. Ele permite que a função forneça um valor útil que pode ser usado em outras partes do programa.  
    > Além de indicar o valor de retorno, o `return` também finaliza a execução da função. Isso significa que quando a função alcança uma instrução `return`, ela é encerrada imediatamente e qualquer código subsequente não é executado. [^chat-gpt]
