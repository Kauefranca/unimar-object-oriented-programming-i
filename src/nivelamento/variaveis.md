# Variáveis

--- Explique para uma criança o que é uma variável em programação

>Claro! Vou explicar o que é uma variável em programação de uma forma fácil de entender.
>
>Imagine que você tem uma caixinha onde pode guardar diferentes coisas. Essa caixinha é chamada de variável. A variável é como uma etiqueta que você coloca na caixa para poder identificá-la.
>
>Em programação, uma variável é uma forma de guardar informações. É como uma caixinha que pode armazenar diferentes tipos de dados, como números, palavras ou até mesmo outros pedaços de código.
>
>Por exemplo, vamos dizer que você queira guardar a sua idade em uma variável. Você pode criar uma variável chamada "idade" e atribuir a ela o valor da sua idade. Digamos que você tenha 10 anos, então você escreveria:
>
>```makefile
>idade = 10
>```
>
>Agora, sempre que você quiser se referir à sua idade no programa, você pode usar a variável "idade". É como se você estivesse pegando a informação guardada na caixinha chamada "idade".
>
>As variáveis também podem ser úteis quando queremos fazer cálculos ou armazenar resultados temporariamente. Elas nos permitem armazenar valores e usar esses valores em diferentes partes do programa.
>
>Então, resumindo, uma variável em programação é uma caixinha onde podemos guardar diferentes informações, como números ou palavras, para usar mais tarde. Elas nos ajudam a organizar e trabalhar com dados dentro de um programa.
> [^chat-gpt]

---

> Variável é o nome utilizado para definir um ou mais valores que são manipulados pelos programas durante a sua operação.
> O nome “variável” é utilizado por ser um tipo de conteúdo que pode apresentar diferentes valores enquanto o sistema está em execução.
> Tudo dependerá do comando do usuário e o tipo de operação que é realizado.
> [^gaea]

O nome da variável é um apelido que damos para um endereço de memoria no computador,
assim, sempre que usamos esse apelido o programa sabe que deve acessar aquele mesmo endereço de memória.
Para nós humanos é muito mais fácil utilizar nomes ou apelidos, já que os endereços de memoria são apenas números e geralmente bem grandes.

A função principal de uma variável é armazenar informações para que posteriormente possamos realizar alguma computação,
como uma conta, apresentar ao usuário, escrever em algum arquivo e etc.
Para armazenar algum valor em uma variável usamos a operação de atribuição, geralmente representada pelo simbolo `=`, algumas linguagens podem usar `<-`, `<=` ou `:=`.

```
nome da variável = valor da variável
```

Poderíamos ler o código anterior como, atribuir `valor da variável` na variável `nome da variável`, ou, `nome da variável` recebe `valor da variável`.
Se "olhássemos" para essa variável `nome da variável` logo após a execução dessa linha seu conteúdo seria `valor da variável`.

```
nome da variável = um outro valor
```

Poderíamos ler o código anterior como, atribuir `um outro valor` na variável `nome da variável`, ou, `nome da variável` recebe `um outro valor`.
As variáveis não conseguem armazenar mais de um valor ao mesmo tempo, então sempre que atribuímos um novo valor perdemos o anterior.
Assim como no exemplo da caixinha, precisamos esvazia-la antes de colocarmos um novo objeto.
Logo após a execução desse código, o valor anterior `valor da variável` foi descartado para que a variável `nome da variável` recebesse o novo valor `um outro valor`.

Como já dito anteriormente, as variáveis são apelidos para endereços de memória no computador, essa memória em questão é a [memória volátil ou memória RAM](https://pt.wikipedia.org/wiki/Mem%C3%B3ria_de_acesso_aleat%C3%B3rio).
Ela tem a característica de não persistir informações na ausência de energia, ou seja, assim que desligarmos o computador perdemos as informações.
As variáveis também tem a mesma característica, mas as variáveis também são limpas logo após a finalização do programa.
Assim, toda vez que reiniciamos nosso programa as variáveis também são reinicializadas.

Não podemos usar simples variáveis se quisermos persistir informações entre execuções diferentes do programa,
ou entre reinicializações do computador.
Para isso, precisamos utilizar memórias não voláteis, o que geralmente envolve a manipulação de arquivos,
ou bancos de dados.
Mas não se preocupe com isso agora, ainda chegaremos nesse assunto.

Se tiver interesse em saber mais sobre os tipos de memória do computador recomendo a leitura de [Armazenamento_de_dados_de_computador](https://pt.wikipedia.org/wiki/Armazenamento_de_dados_de_computador).

## Introdução aos tipos

> As variáveis são inicializadas quanto ao tipo e quanto ao valor, isso significa que terá de ser definida qual o uso que iremos atribuir a ela.
> As variáveis podem ser do tipo inteira, decimal, caractere, string, data e somente decimal positiva, assim, dependendo do tipo de dados que serão armazenados, tem-se um tipo definido para ela.
> [^usp-ltsi]

Para o computador e para o programador é muito importante saber o tipo da variável.
O tipo define o tamanho, ou seja, o quanto de espaço em memória a variável precisa para ser representada.
Sem isso seria impossível para o computador escolher e reservar endereços de memória.
O tipo também define o comportamento da variável, ou seja, as operações que podemos realizar com ela.
Sem isso o programador pode ficar sem saber o que fazer com a variável e o computador sem saber como fazer.

O tipo das variáveis ficam muito claros em linguagens de tipagem estática,
pois geralmente o programador precisa declarar seu tipo explicitamente,
o mesmo não é inferido pela linguagem de programação.

Segue um exemplo em C/C++ onde é declarada uma variável do tipo `int`, com o nome `valor_inteiro`, iniciando com o valor `0`.

```c
int valor_inteiro = 0;
```

Neste exemplo o programador precisou escrever o nome do tipo `int`,
assim o programa sabe que deve tratar essa variável como um número inteiro.
Em C/C++ também precisamos adicionar um `;` no final de certas linhas.

Já em linguagens de tipagem dinâmica, muitas vezes o tipo da variável é inferido do valor de inicialização.

Segue um exemplo semelhante em python.

```python
valor_inteiro = 0
```

Neste exemplo o programador não precisou escrever `int` ou algo relacionado ao tipo da variável.
Porém, ao inicializar a variável com o valor `0` a linguagem inferiu o tipo como inteiro.
Isso não aconteceria se o valor fosse um número com casas decimais ou um texto.

Segue um exemplo em python declarando variáveis de tipos diferentes.

```python
valor_int = 0
valor_float = 0.
valor_str = '0'
```

Por mais que os nomes das variáveis tenham um sufixo (`_int`, `_float`, `_str`) com o nome do tipo,
o python só usa o valor para inferir o tipo,
ignorando o nome da variável.
Para conferir podemos utilizar a funções `type` e `print`,
ainda vamos falar sobre funções,
mas por enquanto só é necessário saber o seguinte:

- A função `type` é utilizada para verificar o tipo da variável, saber que tipo o python inferiu
- A função `print` é utilizada para enviar algo para a saída do programa, no caso, escrever no [terminal/console](https://pt.wikipedia.org/wiki/Interface_de_linha_de_comandos).

```python
valor_int = 0
valor_float = 0.
valor_str = '0'
print(type(valor_int))
print(type(valor_float))
print(type(valor_str))
```

A saída do programa será:

```console
<class 'int'>
<class 'float'>
<class 'str'>
```

Neste exemplo o python consegue distinguir os tipos pelas pequenas diferenças nos valores utilizados,
apenas `0` estamos falando de um número inteiro,
`0.` estamos falando de um número que pode ter casas decimais e
`'0'` estamos falando de um texto.

Não se preocupe em decorar tudo agora, ainda vamos falar mais sobre os tipos das variáveis.
Tenha em mente que usamos variáveis para armazenar valores que precisamos usar posteriormente,
por isso damos nomes, com o nomes podemos fazer alguma referência a essas variáveis e usar seus valores.
Quando estamos resolvendo problemas complexos, geralmente o fazemos por etapas,
então usamos variáveis para armazenar o resultado de cada etapa.
Não que não seja possível resolver um problema complexo de uma vez só sem variáveis,
mas provavelmente seria mais difícil para um humano elaborar, compreender e ler.

Até agora usamos apenas valores fixos em nossas variáveis, ou o que chamamos de [literais](https://pt.wikipedia.org/wiki/Literal_(programa%C3%A7%C3%A3o_de_computadores)),
mas mais a frente vamos ver como colocar outros valores nas variáveis,
como resultados de contas, entradas fornecidas pelo usuário e etc.

[^chat-gpt]: <https://chat.openai.com/>

[^gaea]: <https://gaea.com.br/variaveis-programacao/>

[^usp-ltsi]: <https://sites.usp.br/ltsi/variaveis/>

---

## Exercícios de fixação

{{#quiz ./quiz-variaveis.toml}}

---

## Dúvidas frequentes

- Por quê string tem aspas?

    Para que o computador entenda que estamos falando de um texto e não de mais código,
    precisamos de um identificador, neste caso as aspas.
    Sem um identificador seria muito difícil distinguir um texto de um nome de variável,
    ou um número de sua representação textual.
