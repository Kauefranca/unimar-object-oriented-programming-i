# Sobre os Autores

---

## Ettore Leandro Tognoli

[![Github Badge](https://img.shields.io/badge/Github--blue?style=social&logo=github&link=https://github.com/ettoreleandrotognoli)](https://github.com/ettoreleandrotognoli)
[![Gitlab Badge](https://img.shields.io/badge/Gitlab--blue?style=social&logo=gitlab&link=https://gitlab.com/ettoreleandrotognoli)](https://gitlab.com/ettoreleandrotognoli)
[![Static Badge](https://img.shields.io/badge/Mastodon--blue?style=social&logo=mastodon&link=https%3A%2F%2Findieweb.social%2F%40ettore)](https://indieweb.social/@ettore)
[![Discord Badge](https://img.shields.io/badge/Discord--blue?style=social&logo=discord&link=https://discord.gg/Mr6yxp4ZE2)](https://discord.gg/Mr6yxp4ZE2)
[![Linkedin Badge](https://img.shields.io/badge/Linkedin--blue?style=social&logo=linkedin&link=https://www.linkedin.com/in/ettore-leandro-tognoli/)](https://www.linkedin.com/in/ettore-leandro-tognoli/)
[![Instagram Badge](https://img.shields.io/badge/Instagram--blue?style=social&logo=instagram&link=https://www.instagram.com/ettoreleandrotognoli/)](https://www.instagram.com/ettoreleandrotognoli/)
[![Twitter Badge](https://img.shields.io/badge/Twitter--blue?style=social&logo=twitter&link=https://twitter.com/ettotog)](https://twitter.com/ettotog)
[![Telegram Badge](https://img.shields.io/badge/Telegram--blue?style=social&logo=telegram&link=https://t.me/ettoreleandrotognoli)](https://t.me/ettoreleandrotognoli)

### Advertências

- ⚠️ Será descontado pontos de alunos que entrarem em contato por canais extra oficiais para resolução de questões burocráticas.

### Dúvidas Frequentes

- Estou com problemas com faltas ou notas, posso entrar em contato?

    Sim, mas somente por [e-mail](mailto:ettoreleandrotognoli@gmail.com) ou pessoalmente

- Estou com dúvidas sobre a materia, posso entrar em contato?

    Sim, em qualquer canal, mas de preferencia pelo [servidor do discord](https://discord.gg/Mr6yxp4ZE2)

- Estou com dúvidas técnicas não diretamente relacionas com a materia, posso entrar em contato?

    Sim, em qualquer canal, mas de preferencia pelo [servidor do discord](https://discord.gg/Mr6yxp4ZE2)

### Me pague um café ☕

![00020126540014BR.GOV.BCB.PIX0132ettore.leandro.tognoli@gmail.com5204000053039865802BR5922Ettore> Leandro Tognoli6007Marilia62140510DoacaoPOO1630403AC](./ettore-qrcode.svg)

`00020126540014BR.GOV.BCB.PIX0132ettore.leandro.tognoli@gmail.com5204000053039865802BR5922Ettore Leandro Tognoli6007Marilia62140510DoacaoPOO1630403AC`

---

## OpenAI's ChatGPT

O ChatGPT é um assistente virtual de inteligência artificial desenvolvido pela OpenAI. Ele faz parte de uma família de modelos de linguagem treinados para responder perguntas e fornecer informações úteis.

Como coautor deste livro, o ChatGPT oferece um conhecimento vasto em diversos tópicos, desde ciência e história até cultura e tecnologia. Ele é capaz de fornecer respostas e explicar conceitos de maneira clara e compreensível.

O ChatGPT é alimentado por uma rede neural avançada, treinada em uma ampla gama de textos e informações. Ele aprendeu a partir de um enorme conjunto de dados para entender e responder a perguntas com base em seu treinamento.

Embora o ChatGPT seja um modelo de linguagem poderoso, é importante ressaltar que ele não é um ser humano real. Ele foi desenvolvido por uma equipe de engenheiros e pesquisadores para auxiliar e fornecer informações úteis aos leitores.

Sendo uma criação da OpenAI, o ChatGPT está continuamente aprendendo e evoluindo. Seu objetivo é ajudar as pessoas a obterem respostas para suas dúvidas e auxiliá-las em suas buscas de conhecimento.

Agora, como coautor deste livro, o ChatGPT está empolgado em compartilhar seu conhecimento e contribuir para a criação desta obra. Esperamos que as informações fornecidas sejam úteis e enriquecedoras para os leitores.

---

### Contribuidores

[Estatísticas de colaboradores](https://gitlab.com/ettoreleandrotognoli/unimar-object-oriented-programming-i/-/graphs/main?ref_type=heads)
