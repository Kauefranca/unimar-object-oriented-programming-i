FROM rust:latest
RUN cargo install \
    mdbook \
    mdbook-emojicodes \
    mdbook-plantuml \
    mdbook-quiz \
    mdbook-plantuml \
    mdbook-cmdrun
COPY --from=docker:dind /usr/local/bin/docker /usr/local/bin/
RUN cargo install --git https://github.com/ettoreleandrotognoli/mdbook-ocirun.git mdbook-ocirun