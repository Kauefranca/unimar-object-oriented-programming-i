# Programação Orientada a Objetos - I

Material produzido para aulas de Programação Orientada a Objetos - I

Disponível em: <https://ettoreleandrotognoli.gitlab.io/unimar-object-oriented-programming-i/>

---

## Me pague um café ☕

Se meu material contribuiu de alguma forma e se quiser,
você pode me pagar um café via PIX:

![00020126540014BR.GOV.BCB.PIX0132ettore.leandro.tognoli@gmail.com5204000053039865802BR5922Ettore> Leandro Tognoli6007Marilia62140510DoacaoPOO1630403AC](./src/ettore-qrcode.svg)

`00020126540014BR.GOV.BCB.PIX0132ettore.leandro.tognoli@gmail.com5204000053039865802BR5922Ettore Leandro Tognoli6007Marilia62140510DoacaoPOO1630403AC`
